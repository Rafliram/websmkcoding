<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSekolahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolah', function (Blueprint $table) {
            $table->increments('id_smk');
            $table->string('kota');
            $table->string('nama');
            $table->string('website');
            $table->string('jumlah_siswa');
            $table->string('alamat');
            $table->boolean('lab_komp');
            $table->text('permohonan');
            $table->string('fasilitas_lab');
            $table->string('software_diajarkan');
            $table->string('mapel');
            $table->string('nama_penangggungjwb');
            $table->string('email_penangggungjwb');
            $table->string('kontak_penangggungjwb');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolah');
    }
}
