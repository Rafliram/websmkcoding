<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Smk Coding</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- ICON -->
    <link rel="icon" href="{{ URL::asset('/smkcoding_logo.svg') }}" type="image/x-icon" />

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">

</head>

<body>
    <div id="root_app"></div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/swiper.js') }}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Initialize Swiper Config -->
    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 3.5,
            centeredSlides: true,
            spaceBetween: 0,
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    </script>

    <!-- NAVBAR DEFAULT VALUE -->
    <script>
        $(window).scroll(function() {
            if ($(window).scrollTop() <= 100) {
                $('.navbar').removeClass('navbar-scroll');
            } else {
                $('.navbar').addClass('navbar-scroll');
            }
        });
    </script>

</body>

</html>