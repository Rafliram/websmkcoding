const urlAddress = "http://localhost/websmkcoding/public/images/local/";

export const IMAGE = {
    PIC1 : `${urlAddress}pic1.jpg`,
    PIC2 : `${urlAddress}pic2.jpg`,
    PIC3 : `${urlAddress}pic3.jpg`,
    PIC4 : `${urlAddress}pic4.jpg`,
    BACKGROOUND_DOODLE: `${urlAddress}/doodle_background.png'`,
    BACKGROOUND_DOTTED: `${urlAddress}/dotted_background.png'`,
    BACKGROOUND_AROUND_DOODLE: `${urlAddress}/doodle_around_background.png`,
    POLYGON: `${urlAddress}/polygon.png`,
};

export const ICON = {
    PC : `${urlAddress}/icon/pc.svg`,
    PEOPLE : `${urlAddress}/icon/people.svg`,
    THUMBS_UP : `${urlAddress}/icon/thumbs_up.svg`,
    ANDROID_BLUE : `${urlAddress}/icon/android_blue.png`,
    FIREBASE_BLUE : `${urlAddress}/icon/firebase_blue.png`,
    FLAG_BLUE : `${urlAddress}/icon/flag_blue.png`,
    INTERNET_BLUE : `${urlAddress}/icon/internet_blue.png`,
    BRAIN_WHITE : `${urlAddress}/icon/brain_white.png`,
    IPAD_WHITE : `${urlAddress}/icon/ipad_white.png`,
    LAPTOP_WHITE : `${urlAddress}/icon/laptop_white.png`,
    MOUSE_WHITE : `${urlAddress}/icon/mouse_white.png`,
    ARROW_RIGHT : `${urlAddress}/icon/arrow-right.png`,
    HANDS : `${urlAddress}/icon/hands.svg`,
    MONEY : `${urlAddress}/icon/money.svg`,
    HUGO_AI: `${urlAddress}/Icon/hugo-artificial-intelligence.png`,

    ANDROID_POLYGON: `${urlAddress}/Icon/android_polygon.svg`,
    AZURE_POLYGON:  `${urlAddress}/Icon/azure_polygon.svg`,
    DATASCIENCE_POLYGON: `${urlAddress}/Icon/datascience_polygon.svg`,
    DEV_POLYGON: `${urlAddress}/Icon/dev_polygon.svg`,
    DOTNET_POLYGON: `${urlAddress}/Icon/dotnet_polygon.svg`,
    DUNIAKERJA_POLYGON: `${urlAddress}/Icon/duniakerja_polygon.svg`,
    IOT_POLYGON: `${urlAddress}/Icon/iot_polygon.svg`,
};

export const LOGO = {
    LOGO_HORIZONTAL: `${urlAddress}/smkcoding_logo.svg`,
    LOGO_Vertical: `${urlAddress}/logo_vertical.png`,
    LOGO_NOCAPTION: `${urlAddress}/logo_nocaption.jpeg`,
    KEMKOMINFO: `${urlAddress}/kemkominfo.png`,
    KEMKOMINFO_WHITE: `${urlAddress}/kemkominfo_white.png`,
};