import React from 'react';
import ReactDOM from 'react-dom';

import './assets/style/css/smkcoding-style.css';

import { MainRouter } from './system/Router'

function Index() {
    return (
        <MainRouter/>
    );
}

export default Index;

if (document.getElementById('root_app')) {
    ReactDOM.render(<Index />, document.getElementById('root_app'));
}
