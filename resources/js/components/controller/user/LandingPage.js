import React, { Component } from 'react'

import Navbar from '../../view/components/Navbar'
import LandingPageView from '../../view/user/LandingPageView'
import Footer from '../../view/components/Footer'
import { MHome } from '../../model/page/MLandingPage'

export default class LandingPage extends Component {

    constructor(props) {

        super(props)

        this.state = {
            //HEADER
            header_image: '',
            header_title: '',
            header_description: '',

            // ABOUT
            about_image: '',
            about_tagline: '',
            about_title: '',
            about_description: '',

            //PEMBEKALAN
            pembekalan: [],

            //AFTER GRADUATION  (BENEFIT)
            after_graduate: [],

            //GALLERY
            gallery:[]

        }

        this.method = {

        }

    }

    componentDidMount = () => {
        this.getHeader();
        this.getAbout()
        this.getPembekalan();
        this.getBenefit()
        this.getGallery();
    }

    getHeader = () => {
        MHome.GET("header").then(res => {
            if (MHome.data.status === true) {
                const ref = MHome.data.data.length
                const selectItem = MHome.data.data[ref - 1]
                this.setState({
                    header_image: selectItem.image,
                    header_title: selectItem.judul,
                    header_description: selectItem.deskripsi
                })
            }
        })
            .catch(
                err => {
                    console.log(err, "getAbout");
                }
            );
    }

    getAbout = async () => {

        MHome.GET("about").then(res => {
            if (MHome.data.status === true) {
                const ref = MHome.data.data[0]
                this.setState({
                    about_image: ref.image,
                    about_tagline: ref.tagline,
                    about_title: ref.judul,
                    about_description: ref.deskripsi
                })
            }
        })
            .catch(
                err => {
                    console.log(err, "getAbout");
                }
            );

    }

    getPembekalan = () => {

        MHome.GET("pembekalan").then(res => {
            if (MHome.data.status === true) {
                this.setState({
                    pembekalan: MHome.data.data
                })
            }
        })
            .catch(
                err => {
                    console.log(err, "getAbout");
                }
            );

    }

    getBenefit = () => {
        MHome.GET("benefit").then(res => {
            if (MHome.data.status === true) {
                this.setState({
                    after_graduate: MHome.data.data
                })
            }
        })
            .catch(
                err => {
                    console.log(err, "getAbout");
                }
            );
    }

    getGallery = () => {
        MHome.GET("gallery").then(res => {
            if (MHome.data.status === true) {
                this.setState({
                    gallery: MHome.data.data
                })
            }
        })
            .catch(
                err => {
                    console.log(err, "getAbout");
                }
            );
    }

    render() {
        return (
            <div className="page-container">
                <Navbar />

                <LandingPageView state={this.state} />

                {/* <Footer /> */}
            </div>
        )
    }
}
