import React, { Component } from 'react'
import MentorView from '../../view/user/MentorView'
import Navbar from '../../view/components/Navbar'

export default class Mentor extends Component {

    render() {
        return (
            <div className="page-container">
                <Navbar/>

                <MentorView />
            </div>
        )
    }
}
