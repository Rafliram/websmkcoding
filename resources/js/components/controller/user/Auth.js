import React, { Component } from 'react'
import AuthView from '../../view/user/AuthView'

//Model
import { AUTH } from '../../model/MAuth'
import { CITY } from '../../model/MCity'

export default class Auth extends Component {

    constructor(props) {

        super(props)

        this.state = {
            pagePosition: 'login',
            isLoading: false,
            errMessage: '',

            // LOgin
            email: '',
            password: '',

            // REGISTER SISWA
            student_fullname: '',
            student_gender: '',
            student_phone: '',
            student_school: '',
            student_lesson: '',
            student_class: '',
            student_experience: '',
            android: '',
            student_pcSpecification: '',

            // REGISTER SCHOOL
            smk_name: '',
            smk_website: '',
            student_count: '',
            smk_address: '',
            phone_number: '',
            pc_spesification: '',
            lab_facility: '',
            software_learned: '',
            tech_lesson: '',
        }

        this.method = {
            goToPage: this.goToPage.bind(this),
            _setForm: this._setForm.bind(this),
            registerStudent: this._registerStudent.bind(this),
            login: this._login.bind(this)
        }

    }

    componentDidMount = () => {
        this.pageStats()
        this.getCityList()
    }

    _registerStudent = async (event) => {
        this.spinnerStats()

        const formData = {
            nama: this.state.student_fullname,
            jenisKelamin: this.state.student_gender,
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.password,
            noHp: parseInt(this.state.student_phone),
            sekolahId: 1,// <---- FIX THIS PLZ
            kelas: this.state.student_class,
            lamaBelajarPemograman: this.state.student_experience,
            belajarAndroid: this.state.android
        }

        if (isNaN(formData.noHp)) return this.setState({ errMessage: "Nomor Hp salah" }, () => this.spinnerStats())

        await AUTH.REGISTER(formData).then(res => {

            if (AUTH.data.status === true) {
                console.log(AUTH.data)
            } else {
                console.log(AUTH.data)
                this.setState({ errMessage: AUTH.data.message }, () => this.spinnerStats())
            }

        }).catch(
            err => {
                console.log(err);
            }
        );

    }

    _registerSchool = async (event) => {

        this.spinnerStats()

        const formData = {
            kotaid: 3573, // <-- FIX DIS DUDE
            nama: this.state.smk_name,
            website: this.state.smk_website,
            jumlahSiswa: this.state.student_count,
            alamat: this.state.smk_address,
            lab_facility: this.state.lab_facility,  // <-- 100% NOT SURE BOUT THIS

        }

        if (isNaN(formData.noHp)) return this.setState({ errMessage: "Nomor Hp salah" }, () => this.spinnerStats())

        await AUTH.REGISTER_SCHOOL(formData).then(res => {

            if (AUTH.data.status === true) {
                console.log(AUTH.data)
            } else {
                console.log(AUTH.data)
                this.setState({ errMessage: AUTH.data.message }, () => this.spinnerStats())
            }

        }).catch(
            err => {
                console.log(err);
            }
        );

    }

    _login = async (e) => {

        this.spinnerStats();

        const formData = {
            email: this.state.email,
            password: this.state.password
        }

        if (formData.email === '' && formData.password === '') return this.setState({ errMessage: "Mohon masukan email dan password dengan benar" }, () => this.spinnerStats())

        await AUTH.LOGIN(formData).then(res => {

            if (AUTH.data.status === true) {
                this.setState({ errMessage: "SUCCESS BANG" }, () => this.spinnerStats())
                console.log(AUTH.data)
            } else {
                this.setState({ errMessage: "Email atau password anda salah!" }, () => this.spinnerStats())
                console.log(AUTH.data)
            }

        })
            .catch(
                err => {
                    console.log(err);
                    this.spinnerStats()
                }
            );
    }

    getCityList = async () => {

        await CITY.GET().then(res => {

            if (CITY.data.status === true) {

                

            }

        })
            .catch(
                err => {
                    console.log(err, "getCity");
                }
            );

    }

    _setForm = (e) => {
        this.setState({
            [e.target.name]: [e.target.value].toLocaleString()
        })
    }

    /**
     * Change if spinner visible or not
     */
    spinnerStats = () => {
        this.setState({ isLoading: !this.state.isLoading })
    }

    /**
     * Get url whether login or register
     */
    pageStats = () => {
        const url = this.props.location.pathname
        const split = url.split('/')

        if (split[2] === undefined) {

            this.props.history.push('auth/login')

        } else {

            this.setState({ pagePosition: split[2] })

        }

    }

    goToPage = (link) => {

        this.props.history.push(link)

        this.setState({ pagePosition: link }, console.log("Switch form"))

    }

    render() {
        return (
            <AuthView state={this.state} method={this.method} />
        )
    }
}
