import React, { Component } from 'react'
import ProgramView from '../../view/user/ProgramView'

import Navbar from '../../view/components/Navbar'

export default class Program extends Component {
    render() {
        return (
            <div className="page-container">
                <Navbar />

                <ProgramView />
            </div>
        )
    }
}
