import React, { Component } from 'react'
import Navbar from '../../view/components/Navbar'

import NewsView from '../../view/user/NewsView'

export default class News extends Component {
    render() {
        return (
            <div className="page-container">
                <Navbar/>
                <NewsView/>
            </div>
        )
    }
}
