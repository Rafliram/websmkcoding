import React, { Component } from 'react'
import Navbar from '../../view/components/Navbar'
import AboutView from '../../view/user/AboutView'

export default class About extends Component {
    render() {
        return (
            <div style={{height:'100%'}}>
                <Navbar/>

                <AboutView/>
            </div>
        )
    }
}
