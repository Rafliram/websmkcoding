import React, { Component } from 'react'
import SearchSchoolView from '../../view/user/SearchSchoolView'
import Navbar from '../../view/components/Navbar'

export default class SearchSchool extends Component {
    render() {
        return (
            <div className="page-container">
                <Navbar />

                <SearchSchoolView />

            </div>
        )
    }
}
