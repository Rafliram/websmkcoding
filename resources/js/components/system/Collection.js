/**
 * USER DATA KEY
 */
export const AuthKey = {
    IS_LOGGED: "@SMKCODING:IS_LOGGED",
    LOGIN_DATA : "@SMKCODING:LOGIN_DATA",
};

export const String = {
    SMKCODING_WEB_BASE: 'http://localhost/websmkcoding/public/',
    SMKCODING_API_BASE: `http://localhost/websmkcoding/public/api/`,
    // SMKCODING_API_BASE: `http://${window.location.hostname}/api/`,
    // SMKCODING_STORAGE_BASE: `http://${window.location.hostname}/storage/`,
};
