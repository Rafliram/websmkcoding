import React from 'react';

import { BrowserRouter as Router, Route, Switch, useRouteMatch, Link } from 'react-router-dom';

/**
 * GLOBAL COMPONENT
 */
import '../assets/style/css/admin/sb-admin-2.min.css';
import {LOGO} from '../assets/Images';
import Topbar from '../view/components/admin/Topbar';

/**
 * SCREEN
 */
import LandingPage from '../controller/user/LandingPage';
import Auth from '../controller/user/Auth';

/**
 * SCREEN ADMIN
 */
import Dashboard from '../controller/Admin/Dashboard';
import ManageIdentitas from '../controller/Admin/ManageIdentitas';


import News from '../controller/user/News'
import About from '../controller/user/About';
import Mentor from '../controller/user/Mentor'
import SearchSchool from '../controller/user/SearchSchool';
import Program from '../controller/user/Program';

export const MainRouter = () => {

    return <Router>

        <Switch>

            <Route exact path='/' component={LandingPage} />

            <Route path='/auth' component={Auth} />
            <Route path='/about' component={About} />
            <Route path='/mentor' component={Mentor} />
            <Route path='/program' component={Program} />
            <Route path='/news' component={News} />
            <Route path='/smkterdaftar' component={SearchSchool} />

            <Route component={LandingPage} />

        </Switch>

    </Router>

}

export const AdminRouter = () => {

    let match = useRouteMatch();

    return <Router>
        <div id="wrapper">

            {/* SIDEBAR */}
            <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                {/* <!-- Sidebar - Brand --> */}
                <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                    <div className="sidebar-brand-icon ">
                        <img src={LOGO.LOGO_HORIZONTAL} style={{ width: '100%', marginLeft: 10 }} />
                    </div>
                    <div className="sidebar-brand-text mx-3">
                    </div>
                </a>

                {/* <!-- Divider --> */}
                <hr className="sidebar-divider my-0" />

                {/* <!-- Nav Item - Dashboard --> */}
                <li className="nav-item active">
                    <Link to={'/admin'} className="nav-link" href="index.html">
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </Link>
                </li>

                {/* <!-- Divider --> */}
                <hr className="sidebar-divider" />

                {/* <!-- Heading --> */}
                <div className="sidebar-heading">
                    Interface
                </div>

                {/* <!-- Nav Item - Manage Siswa Collapse Menu --> */}
                <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseManageSiswa" aria-expanded="true" aria-controls="collapseTwo">
                        <i className="fas fa-fw fa-cog"></i>
                        <span>Manage Siswa</span>
                    </a>
                    <div id="collapseManageSiswa" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Manage Siswa:</h6>
                            <Link to={`${match.path}/admin/manageidentitas`} className="collapse-item">Manage Identitas</Link>
                            <Link to={`${match.path}/admin/portofolio`} className="collapse-item">Manage Portofolio</Link>
                        </div>
                    </div>
                </li>

                {/* <!-- Nav Item - Utilities Collapse Menu --> */}
                <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                        <i className="fas fa-fw fa-wrench"></i>
                        <span>Utilities</span>
                    </a>
                    <div id="collapseUtilities" className="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Edit:</h6>
                            <Link className="collapse-item" to="admin/materi" >Materi</Link>
                        </div>
                    </div>
                </li>

                {/* <!-- Divider --> */}
                <hr className="sidebar-divider" />

                {/* <!-- Heading --> */}
                <div className="sidebar-heading">
                    Addons
                </div>

                {/* <!-- Nav Item - Pages Collapse Menu --> */}
                <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                        <i className="fas fa-fw fa-folder"></i>
                        <span>Pages</span>
                    </a>
                    <div id="collapsePages" className="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Docoment:</h6>
                            <Link className="collapse-item" to={`${match.url}/event`} >Event</Link>
                            <Link className="collapse-item" to={`${match.url}/galery`} >Galery</Link>
                        </div>
                    </div>
                </li>

                {/* <!-- Nav Item - Charts --> */}
                <li className="nav-item">
                    <Link to={`${match.url}/chart`} className="nav-link" href="charts.html">
                        <i className="fas fa-fw fa-chart-area"></i>
                        <span>Charts</span>
                    </Link>
                </li>

                {/* <!-- Nav Item - Tables --> */}
                <li className="nav-item">
                    <Link to={`${match.url}/tables`} className="nav-link" href="tables.html">
                        <i className="fas fa-fw fa-table"></i>
                        <span>Tables</span>
                    </Link>
                </li>

                {/* <!-- Divider --> */}
                <hr className="sidebar-divider d-none d-md-block" />


            </ul>
            {/* end of sidebar */}

            {/* <!-- Content Wrapper --> */}
            <div id="content-wrapper" className="d-flex flex-column">

                {/* <!-- Main Content --> */}
                <div id="content">

                    <Topbar />

                    {/* <!-- Begin Page Content --> */}
                    <div className="container-fluid">

                        <Switch>

                            <Route exact path={`/admin`} component={Dashboard} />
                            <Route path={`${match.path}/admin/manageidentitas`} component={ManageIdentitas} />

                        </Switch>

                    </div>

                </div>

            </div>

        </div>

    </Router>
}
