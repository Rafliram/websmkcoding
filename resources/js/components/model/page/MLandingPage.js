import { String } from '../../system/Collection';

export var data = []

export var MHome = {

    data: [],

    GET: function (url) {

        this.data = [];

        var apiUrl = String.SMKCODING_API_BASE + url;

        console.log(apiUrl, "Auth HEADER@GET");

        return new Promise((resolve, reject) => {

            fetch(apiUrl, {
                method: "GET",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
            })
                .then((response) => response.json())
                .then((responseData) => {

                    if (responseData) {

                        this.data = responseData;
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });

        });

    },
}