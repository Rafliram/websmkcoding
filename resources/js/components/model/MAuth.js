import { String } from '../system/Collection';

export var data = []

export var AUTH = {

    data: [],

    LOGIN: function (formData) {

        this.data = [];

        var apiUrl = String.SMKCODING_API_BASE + 'user/login';

        console.log(apiUrl, "Auth LOGIN@POST");

        return new Promise((resolve, reject) => {

            fetch(apiUrl, {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email   : formData.email,
                    password: formData.password
                })
            })
                .then((response) => response.json())
                .then((responseData) => {

                    if (responseData) {

                        this.data = responseData;
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });

        });

    },

    REGISTER: function (formData) {

        this.data = [];

        var apiUrl = String.SMKCODING_API_BASE + 'user/register';

        console.log(apiUrl, "USER REGISTER@POST");

        console.log(formData,"FORMDATA")

        return new Promise((resolve, reject) => {

            fetch(apiUrl, {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formData)
            })
                .then((response) => response.json())
                .then((responseData) => {

                    if (responseData) {

                        this.data = responseData;
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });

        });

    },

    REGISTER_SCHOOL: function (formData) {

        this.data = [];

        var apiUrl = String.SMKCODING_API_BASE + 'sekolah/register';

        console.log(apiUrl, "USER REGISTER@POST");

        console.log(formData,"FORMDATA")

        return new Promise((resolve, reject) => {

            fetch(apiUrl, {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formData)
            })
                .then((response) => response.json())
                .then((responseData) => {

                    if (responseData) {

                        this.data = responseData;
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });

        });

    },


}