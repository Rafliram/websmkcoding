import { String } from '../system/Collection';

export var data = []

export var CITY = {

    data: [],

    GET: function (formData) {

        this.data = [];

        var apiUrl = String.SMKCODING_API_BASE + 'kota';

        console.log(apiUrl, "Auth GET@POST");

        return new Promise((resolve, reject) => {

            fetch(apiUrl, {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
            })
                .then((response) => response.json())
                .then((responseData) => {

                    if (responseData) {

                        this.data = responseData;
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });

        });

    },

}