import React, { Component } from 'react'

import PrimaryButton from '../components/button/PrimaryButton';
import { LOGO, IMAGE, ICON } from '../../assets/Images';
import Separator from '../components/Separator';

export default class LandingPageView extends Component {
    render() {
        return (
            <div id="landing-page">

                <div className="head">

                    <div className="container">

                        <div className="row no-gutter">

                            <div className="col-lg-6 col-md-12 col-12 caption order-lg-1 order-2">

                                <div className="subtitle">
                                    <h5>digital</h5>
                                </div>

                                <h1>
                                    {this.props.state.header_title}
                                </h1>

                                <div className="mt-5 action-button">
                                    <PrimaryButton title={this.props.state.header_description} stripe />
                                </div>

                                <p className="mt-5">
                                    <img src={LOGO.KEMKOMINFO} className="small-logo"></img>
                                    KEMKOMINFO
                                </p>

                            </div>

                            <div className="col-lg-6 col-md-12 col-12 image-group order-lg-2 order-1">

                                <div className="image-wrapper my-auto mx-md-auto">

                                    <div className="dominate">

                                        <div className="diamond-container white-outline">

                                            <img src={IMAGE.PIC2} className="diamond-image" />

                                        </div>

                                        {/* ACCENT */}
                                        <div className="line1"></div>

                                    </div>

                                    <div className="shadowed">

                                        <div className="diamond-container">
                                            <img src={IMAGE.PIC3} className="diamond-image" />
                                        </div>

                                        {/* 
                                        Becarefull this code below is fragile
                                        proceed with extreme caution
                                        */}

                                        {/* ACCENT */}
                                        <div className="line1"></div>
                                        <div className="line2"></div>
                                        <div className="triangle-down"></div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div className="container my-5">
                    <Separator />
                </div>

                <div className="about-us">

                    <div className="container">

                        <div className="row no-gutter justify-content-center align-items-center">

                            <div className="col-lg-6 col-md-4 col-12 text-center">

                                <img src={this.props.state.about_image} />

                            </div>

                            <div className="col-lg-6 col-md-8 col-12 mt-md-0 mt-lg-0 mt-5">

                                <h6>{this.props.state.about_tagline}</h6>

                                <h3>{this.props.state.about_title}</h3>

                                <p>
                                    {this.props.state.about_description}
                                </p>

                            </div>

                        </div>

                    </div>

                </div>

                <div className="pembekalan">

                    <div className="background-wrapper">
                        <h3>Pembekalan</h3>
                    </div>

                    <div className="container">

                        <div className="card">

                            <div className="row no-gutter">

                                {
                                    this.props.state.pembekalan.map((item, id) => {
                                        return <div key={id} className="col-lg-4 col-md-4 col-12 item text-center">
                                            <img src={item.image} />
                                            <h3>{item.title}</h3>
                                        </div>
                                    })
                                }

                            </div>

                            <div className="mt-5 mb-4 mx-auto action-btn" style={{ width: 'max-content' }}>
                                <PrimaryButton center title="Program SMKCODING" />
                            </div>

                        </div>

                    </div>

                </div>

                <div className="container my-5">
                    <Separator />
                </div>

                <div className="materi-pembelajaran">

                    <div className="container text-center">

                        <h1>Materi Pembelajaran</h1>
                        <p>Tahun 2020</p>

                        <div className="row no-gutter">

                            {/* Dekstop , Tablet View */}
                            <div className="col-md-6 pr-5 text-right default-view">

                                <div className="materi">
                                    <h3>
                                        Android
                                        <img src={ICON.ANDROID_BLUE} className="icon-right" />
                                    </h3>
                                    <p className="p-right">
                                        Karena OS ini diandalkan oleh mayoritas pengguna smartphone
                                        di Indonesia, orang yang bisa ngoding Android sangat dicari
                                        dan dibutuhkan.
                                    </p>
                                </div>

                                <div className="materi"></div>

                                <div className="materi">
                                    <h3>
                                        Internet Of Things
                                        <img src={ICON.INTERNET_BLUE} className="icon-right" />
                                    </h3>
                                    <p className="p-right">
                                        Kenalan dengan konsep teknologi masa depan di mana semua perangkat bisa
                                        saling terhubung, dan melakukan hal secara otomatis
                                    </p>
                                </div>

                                <div className="materi"></div>

                            </div>

                            <div className="col-md-6 pl-5 text-left default-view">

                                <div className="materi"></div>

                                <div className="materi">
                                    <h3>
                                        <img src={ICON.FIREBASE_BLUE} className="icon-left" />
                                        Firebase
                                    </h3>
                                    <p className="p-left">
                                        Sistem database noSQL yang mempermudah dalam pembuatan
                                        beckend mobile atau web site.
                                    </p>
                                </div>

                                <div className="materi"></div>

                                <div className="materi">
                                    <h3>
                                        <img src={ICON.FLAG_BLUE} className="icon-left" />
                                        Persiapan Akhir
                                    </h3>
                                    <p className="p-left">
                                        Dari ribuan talenta yang ada, mengapa perusahaan idamanmu harus memilih kamu?
                                        Kuasai cara menampilkan kemampuan diri pada industri dengan meyakinkan.
                                    </p>
                                </div>

                            </div>

                            {/* Mobile View */}
                            <div className="col-md-6 text-right mobile-view">

                                <div className="materi">
                                    <h3>
                                        <img src={ICON.ANDROID_BLUE} className="icon-left" />
                                        Android
                                    </h3>
                                    <p className="p-right">
                                        Karena OS ini diandalkan oleh mayoritas pengguna smartphone
                                        di Indonesia, orang yang bisa ngoding Android sangat dicari
                                        dan dibutuhkan.
                                    </p>
                                </div>

                                <div className="materi">
                                    <h3>
                                        <img src={ICON.INTERNET_BLUE} className="icon-left" />
                                        Internet Of Things
                                    </h3>
                                    <p className="p-right">
                                        Kenalan dengan konsep teknologi masa depan di mana semua perangkat bisa
                                        saling terhubung, dan melakukan hal secara otomatis
                                    </p>
                                </div>

                                <div className="materi">
                                    <h3>
                                        <img src={ICON.FIREBASE_BLUE} className="icon-left" />
                                        Firebase
                                    </h3>
                                    <p className="p-right">
                                        Sistem database noSQL yang mempermudah dalam pembuatan
                                        beckend mobile atau web site.
                                    </p>
                                </div>

                                <div className="materi">
                                    <h3>
                                        <img src={ICON.FLAG_BLUE} className="icon-left" />
                                        Persiapan Akhir
                                    </h3>
                                    <p className="p-right">
                                        Dari ribuan talenta yang ada, mengapa perusahaan idamanmu harus memilih kamu?
                                        Kuasai cara menampilkan kemampuan diri pada industri dengan meyakinkan.
                                    </p>
                                </div>

                            </div>

                            <div className="timeline">
                                <div className="circle"></div>
                                <div className="line"></div>
                                <div className="circle"></div>
                                <div className="line"></div>
                                <div className="circle"></div>
                                <div className="line"></div>
                                <div className="circle"></div>
                            </div>

                        </div>

                    </div>

                </div>

                <div className="container my-5">
                    <Separator />
                </div>

                <div className="mentor">

                    <div className="container text-center">

                        <h1>Mentor Praktisi</h1>
                        <p className="subtitle">Mentor - mentor terlatih yang sudah berpengalaman</p>

                        <div className="row no-gutter align-items-center">

                            <div className="col-lg-6 col-md-6 col-12">

                                <div className="image-group">

                                    <img src={IMAGE.PIC4} className="mentor-pic" />

                                    {/* ACCENT */}
                                    <div className="circle1"></div> {/*  big */}
                                    <div className="circle2"></div> {/*  small */}
                                    <img src={IMAGE.BACKGROOUND_DOTTED} className="accent" />

                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6 col-12 text-left mt-lg-0 mt-md-0 mt-5">

                                <h6>Adnroid Spesialis</h6>

                                <div className="subtitle">

                                    <h5>Mas Andre</h5>

                                </div>

                                <p>
                                    Jangan cuma asah
                                    kemampuan, tetapi juga gali
                                    dan pelajari pengalaman
                                    mereka, orang-orang
                                    terhebat dalam industri
                                    teknologi di kotamu.
                                </p>

                                <div className="button-group">
                                    <button className="left mr-2">
                                        <img src={ICON.ARROW_RIGHT} />
                                    </button>
                                    <button className="right ml-2">
                                        <img src={ICON.ARROW_RIGHT} />
                                    </button>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div className="container my-5">
                    <Separator />
                </div>

                <div className="after-graduate">

                    <div className="container">

                        <div className="row no-gutter align-items-center">

                            <div className="col-lg-4 col-md-6 col-12">

                                {
                                    this.props.state.after_graduate.map((item, id) => {
                                        if (id === 0) {
                                            return  <div key={id} className="subtitle">

                                            <h5>{item.tagline}</h5>
        
                                            <h1>
                                                {item.judul}
                                            </h1>
        
                                            <p>
                                                {item.deskripsi}
                                            </p>
        
                                        </div>
                                        }

                                    })
                                }

                            </div>

                            <div className="col-lg-8 col-md-6 col-12">

                                <div className="row no-gutter">

                                    {
                                        this.props.state.after_graduate.map((item, id) => {
                                            if (id !== 0) {
                                                return <div key={id} className="col-lg-6 col-md-12 col-12">

                                                    <div className={`item ${id === 1 ? "space-top" : ""}`}>

                                                        <img src={item.image} />
                                                        <h6>
                                                            {item.judul}
                                                        </h6>
                                                        <p>
                                                            {item.deskripsi}
                                                        </p>

                                                    </div>

                                                </div>
                                            }

                                        })
                                    }

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div className="container my-5">
                    <Separator />
                </div>

                <div className="galery">

                    <div className='container text-center'>

                        <h1>Gallery</h1>
                        <p>Lihat kawan seperjuanganmu</p>

                        <div className="row no-gutter gallery-container">

                            <div className="col-lg-6 col-md-6 col-12">

                                {
                                    this.props.state.gallery.map((item, id) => {
                                        if (id === 0) {
                                            return <div key={id}>
                                                <img src={item.image} className="video" />
                                            </div>
                                        }
                                    })
                                }

                            </div>

                            <div className="col-lg-6 col-md-6 col-12 mt-lg-0 mt-md-0 mt-4">

                                <div className="row no-gutter">

                                    {
                                        this.props.state.gallery.map((item, id) => {
                                            if (id !== 0) {
                                                return <div key={id} className="col-md-6 col-12 mb-4">
                                                    <img src={item.image} className="pic" />
                                                </div>
                                            }
                                        })
                                    }

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div className="daftar-sekolah">

                    <div className="container my-auto">

                        <div className="subtitle">
                            <h5>daftarkan sekolahmu</h5>
                        </div>

                        <h1>Ingin agar SMK Coding <span>diadakan di sekolah Anda?</span></h1>

                        <div className="btn-container">
                            <PrimaryButton light title="DAFTAR SEKARANG" />
                        </div>

                    </div>

                </div>

                {/* <div className="space"></div> */}

            </div >
        )
    }
}
