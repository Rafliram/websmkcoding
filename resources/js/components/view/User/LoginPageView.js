import React, { Component } from 'react';

class LoginPageView extends Component {
    render() {
        return (
            <div className="login">

                    <div className="background-wrapper">
                        <h3>Login</h3>
                    </div>

                    <div className="container">

                        <div className="card">

                            <div className="row no-gutter">

                                <div className="col-lg-4 col-md-4 col-12 item text-center">
                                    <img src={ICON.PC} />
                                    <h3>Kemampuan Teknis Sesuai Kebutuhan Industri</h3>
                                </div>

                                <div className="col-lg-4 col-md-4 col-12 item text-center">
                                    <img src={ICON.THUMBS_UP} />
                                    <h3>Kemampuan Soft Skill</h3>
                                </div>

                                <div className="col-lg-4 col-md-4 col-12 item text-center">
                                    <img src={ICON.PEOPLE} />
                                    <h3>Channel Praktisi & Pegiat Industri</h3>
                                </div>

                            </div>

                            <div className="mt-5 mb-4 mx-auto" style={{ width: 'max-content' }}>
                                <PrimaryButton center title="Login" />
                            </div>

                        </div>

                    </div>

                </div>

        );
    }
}

export default LoginPageView;