import React, { Component } from 'react'

import { IMAGE } from '../../assets/Images'
import Separator from '../components/Separator'
import PrimaryButton from '../components/button/PrimaryButton'
import RoundedLabel from '../components/RoundedLabel'

export default class MentorView extends Component {
    render() {
        return (
            <div id="mentor">

                <div id="head" >

                    <div className="container my-auto">

                        <div className="row align-items-center ">

                            <div className="col-lg-5 col-md-12 col-12 image-container">

                                <img src={IMAGE.PIC1} className="hero"></img>

                                <div className="square-accent"></div>
                                <div className="count-accent">01</div>

                            </div>

                            <div className="col-lg-7 col-md-12 col-12 mt-lg-0 mt-5 caption-container">

                                <div className="background">

                                    <div className="subtitle">
                                        <h5>digital</h5>
                                    </div>

                                    <h1>
                                        Belajar dari <br />
                                        para <span>ahli</span> <br />
                                        di bidang industri.
                                    </h1>

                                    <p>
                                        Mereka adalah orang-orang terbaik di industri teknologi yang siap turun tangan untuk mengasah keahlian programming dan kesiapan kariermu dalam 5 pekan!.
                                    </p>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div className="container my-5">
                    <Separator />
                </div>

                <div id="mentor-list">

                    <div className="container text-center">

                        <h1>Mentor Kita</h1>

                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center active" target="_blank">IoT</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" target="_blank">Android</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" target="_blank">All Mentor</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" target="_blank">Motivator</a>
                            </li>
                        </ul>

                    </div>

                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div className="pic-wrapper">
                                    <img src={IMAGE.PIC1} className="mentor-image" />
                                    <div className="category">
                                        Android
                                    </div>
                                </div>
                                <div className="mt-4">
                                    <h2>ADE FAJR ARIAV</h2>
                                    <RoundedLabel title="MYSQL Database" />
                                    <RoundedLabel title="Firebase" />
                                    <RoundedLabel title="Linux" />
                                    <RoundedLabel title="PHP" />
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div className="pic-wrapper">
                                    <img src={IMAGE.PIC2} className="mentor-image" />
                                    <div className="category">
                                        Android
                                    </div>
                                </div>
                                <div className="mt-4">
                                    <h2>Ade Rifaldi</h2>
                                    <RoundedLabel title="Android Java" />
                                    <RoundedLabel title="MYSQL Database" />
                                    <RoundedLabel title="Java" />
                                    <RoundedLabel title="PHP" />
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div className="pic-wrapper">
                                    <img src={IMAGE.PIC3} className="mentor-image" />
                                    <div className="category">
                                        Android
                                    </div>
                                </div>
                                <div className="mt-4">
                                    <h2>Aditya Putra Kejora</h2>
                                    <RoundedLabel title="MYSQL Database" />
                                    <RoundedLabel title="Kotlin" />
                                    <RoundedLabel title="VB.Net" />
                                    <RoundedLabel title="Java" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        )
    }
}
