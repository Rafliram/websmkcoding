import React, { Component } from 'react'
import SchoolCardROW from '../components/card/SchoolCardROW'

export default class SearchSchoolView extends Component {

    render() {
        return (
            <div id="search-school">

                <div id="search-section">

                    <div className="container">

                        <div className="row align-items-center">

                            <div className="col-12 col-md-6 col-lg-6 box border-separator">
                                <i className="far fa-compass icon"></i>
                                <div className="form-group">
                                    <label>Provinsi</label>
                                    <select className="form-control">
                                        <option>Pilih provinsi...</option>
                                        <option>Jawa Timur</option>
                                        <option>Jawa Barat</option>
                                        <option>Kalimantan Barat</option>
                                        <option>Sumatra</option>
                                    </select>
                                </div>
                            </div>

                            <div className="col-12 col-md-6 col-lg-6 box">
                                <i className="far fa-building icon"></i>
                                <div className="form-group">
                                    <label>Kota</label>
                                    <select className="form-control">
                                        <option>Pilih Kota...</option>
                                        <option>Jawa Timur</option>
                                        <option>Jawa Barat</option>
                                        <option>Kalimantan Barat</option>
                                        <option>Sumatra</option>
                                    </select>
                                </div>
                            </div>


                        </div>

                    </div>

                </div>

                <div id="result-list">

                    <div className="container">

                        <div className="row">

                            <SchoolCardROW
                                col="col-12 col-md-3 col-md-4 mb-4"
                                image="https://images.unsplash.com/photo-1565734777784-6e89609ed871?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                schoolName="SMK Indonesia"
                                headmaster="Dr. H. Sutami, MM"
                                location="Jawa Timur, Malang"
                                address="Jl. Tanimbar No.22 Kota Malang" />

                            <SchoolCardROW
                                col="col-12 col-md-3 col-md-4 mb-4"
                                image="https://images.unsplash.com/photo-1582902342515-150ae93901f3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60    "
                                schoolName="SMK Indonesia"
                                headmaster="Dr. H. Sutami, MM"
                                location="Jawa Timur, Malang"
                                address="Jl. Tanimbar No.22 Kota Malang" />

                            <SchoolCardROW
                                col="col-12 col-md-3 col-md-4 mb-4"
                                image="https://images.unsplash.com/photo-1582902373026-dff87d50b672?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                schoolName="SMK Indonesia"
                                headmaster="Dr. H. Sutami, MM"
                                location="Jawa Timur, Malang"
                                address="Jl. Tanimbar No.22 Kota Malang" />

                        </div>

                    </div>

                </div>

            </div>
        )
    }
}
