import React, { Component } from 'react'

import { LOGO } from '../../assets/Images'

/**
 * Components
 */
import InputRounded from '../components/form/InputRounded'
import PrimaryButton from '../components/button/PrimaryButton'
import InputRadioButton from '../components/form/InputRadioButton'

export default class AuthView extends Component {

    renderLogin = () => {
        return <div className="login-card text-center">

            <div className="login-contianer">

                <h1>LOGIN</h1>

                <form className="login-form" onSubmit={(e) => e.preventDefault()}>

                    <div className="input-container"> {/* Determine width of the input */}
                        <InputRounded
                            name="email"
                            placeholder="Email Address"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container mt-4 mb-4"> {/* Determine width of the input */}
                        <InputRounded
                            type="password"
                            name="password"
                            placeholder="Password"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="btn-container">

                        <p className="err-message my-2">
                            {this.props.state.errMessage}
                        </p>

                        <PrimaryButton
                            withLoading
                            loading={this.props.state.isLoading}
                            width="100%"
                            title="Login"
                            onClick={() => this.props.method.login()} />
                    </div>

                    <p>
                        Belum punya akun?
                    <span onClick={() => this.props.method.goToPage('register')}> Daftar </span>
                    </p>

                </form>

            </div>

        </div>
    }

    renderRegister = () => {
        return <div className="register-card text-center">

            <div className="register-container">

                <h1>DAFTAR SISWA</h1>

                <form className="register-form" onSubmit={(e) => e.preventDefault()}>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Nama Lengkap</label>
                        <InputRounded
                            name="student_fullname"
                            onChange={(e) => this.props.method._setForm(e)}/>
                    </div>

                    <div className="input-container">
                        <label className="mr-5 mb-0">Jenis Kelamin</label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_gender"
                                value="L"
                                onChange={(e) => this.props.method._setForm(e)} />Laki - laki
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_gender"
                                value="P"
                                onChange={(e) => this.props.method._setForm(e)} />Perempuan
                        </label>
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Email</label>
                        <InputRounded
                            name="email"
                            placeholder="Email Address"
                            onChange={(e) => this.props.method._setForm(e)}/>
                    </div>

                    <div className="input-container">
                        <label>Password</label>
                        <InputRounded
                            type="password"
                            name="password"
                            placeholder="Password"
                            onChange={(e) => this.props.method._setForm(e)}/>
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>No. Handphone</label>
                        <InputRounded
                            name="student_phone"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Sekolah</label>
                        <InputRounded
                            name="student_school"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Bahasa pemrograman yang pernah dipelajari</label>
                        <InputRounded
                            name="school_lesson"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container">
                        <label className="mr-5 mb-0">Saya Kelas</label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_class"
                                value="1"
                                onChange={(e) => this.props.method._setForm(e)} />1
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_class"
                                value="2"
                                onChange={(e) => this.props.method._setForm(e)} />2
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_class"
                                value="3"
                                onChange={(e) => this.props.method._setForm(e)} />3
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_class"
                                value="Alumni"
                                onChange={(e) => this.props.method._setForm(e)} />Alumni
                        </label>
                    </div>

                    <div className="input-container">
                        <label className="mr-5">Lama belajar pemrograman dasar</label> <br />
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_experience"
                                value="Kurang dari 1 tahun"
                                onChange={(e) => this.props.method._setForm(e)} />Kurang dari 1 tahun
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_experience"
                                value="1 - 2 tahun"
                                onChange={(e) => this.props.method._setForm(e)} />1 - 2 tahun
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_experience"
                                value="Lebih dari 2 tahun"
                                onChange={(e) => this.props.method._setForm(e)} />Lebih dari 2 tahun
                        </label>
                    </div>

                    <div className="input-container">
                        <label className="mr-5">Pernah belajar android?</label> <br />
                        <label className="radio-inline">
                            <InputRadioButton
                                name="android"
                                value="Ya"
                                onChange={(e) => this.props.method._setForm(e)} />Sudah pernah
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="android"
                                value="Tidak"
                                onChange={(e) => this.props.method._setForm(e)} />Belum pernah
                        </label>
                    </div>

                    <div className="input-container">
                        <label className="mr-5">Apakah kamu mempunyai laptop dengan minimum RAM 4 GB?</label> <br />
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_pcSpecification"
                                value="Ya"
                                onChange={(e) => this.props.method._setForm(e)} />Ya
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_pcSpecification"
                                value="Tidak"
                                onChange={(e) => this.props.method._setForm(e)} />Tidak
                        </label>
                        <label className="radio-inline">
                            <InputRadioButton
                                name="student_pcSpecification"
                                value="Tidak, tapi saya bisa meminjam"
                                onChange={(e) => this.props.method._setForm(e)} />Tidak, tapi saya bisa meminjam
                        </label>
                    </div>

                    <div className="btn-container w-75 mx-auto">

                        <p className="err-message my-2">
                            {this.props.state.errMessage}
                        </p>

                        <PrimaryButton
                            withLoading
                            loading={this.props.state.isLoading}
                            width="100%"
                            title="Daftar"
                            onClick={() => this.props.method.registerStudent()} />
                    </div>

                    <p>
                        Sudah punya akun?
                    <span onClick={() => this.props.method.goToPage('login')}> Login </span>
                    </p>

                </form>

            </div>

        </div>
    }

    renderRegisterSchool = () => {
        return <div className="register-card text-center">

            <div className="register-container">

                <h1>DAFTAR SEKOLAH</h1>

                <form className="register-form">

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Nama SMK</label>
                        <InputRounded
                            name="smk_name"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Website SMK</label>
                        <InputRounded
                            name="smk_website"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Berapa jumlah siswa jurusan IT dalam satu angkatan?</label>
                        <InputRounded
                            name="student_count"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Alamat SMK</label>
                        <InputRounded
                            name="smk_address"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>No. Telepon Penanggung Jawab</label>
                        <InputRounded
                            name="phone_number"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container">
                        <label className="mr-5">Apakah SMK Anda mempunyai lab yang komputer minimum untuk Android Studio? (RAM minimal 4 GB)</label> <br />
                        <label className="radio-inline">
                            <input
                                defaultChecked
                                type="radio"
                                name="pc_spesification"
                                onChange={(e) => console.log(e.currentTarget.value)} />Ya
                        </label>
                        <label className="radio-inline">
                            <input
                                type="radio"
                                name="pc_spesification"
                                onChange={(e) => console.log(e.currentTarget.value)} />Tidak
                        </label>
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Fasilitas Lab Komputer SMK Anda?</label>
                        <InputRounded
                            name="lab_faciliity"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Software komputer yang diajarkan di SMK</label>
                        <InputRounded
                            name="software_learned"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="input-container"> {/* Determine width of the input */}
                        <label>Mata pelajaran terkait teknologi apa saja yang diajarkan di SMK Anda?</label>
                        <InputRounded
                            name="tech_lesson"
                            onChange={(e) => this.props.method._setForm(e)} />
                    </div>

                    <div className="btn-container w-75 mx-auto">
                        <PrimaryButton
                            width="100%"
                            title="Daftar" />
                    </div>

                    <p>
                        Sudah punya akun?
                        <span onClick={() => this.props.method.goToPage('login')}> Login </span>
                    </p>

                </form>

            </div>

        </div>
    }

    render() {
        return (
            <div id="auth">

                {/* BACKGROUND */}
                <div className="row no-gutter h-100">

                    {/* LEFT CONTAINER */}
                    <div className="col-6 left-container">
                        <img src={LOGO.LOGO_HORIZONTAL} className="logo" />
                        <div className={this.props.state.pagePosition === 'login' ? 'title' : 'title-small'}>
                            <h1>The Digital Energy of Asia.</h1>
                            <p>Mengembangkan bakat bidang teknologi.</p>
                        </div>
                        {
                            this.props.state.pagePosition === 'login'
                                ?
                                <div className="action">
                                    <i className="fas fa-chevron-left"></i>
                                    {/* Daftar */}
                                    <div className="btn-group">
                                        <button type="button" className="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Daftar
                                        </button>
                                        <div className="dropdown-menu">
                                            <a className="dropdown-item" onClick={() => this.props.method.goToPage('register')}>
                                                <i className="fas fa-user-graduate"></i> Daftar Siswa
                                            </a>
                                            <a className="dropdown-item" onClick={() => this.props.method.goToPage('register-school')}>
                                                <i className="fas fa-school"></i> Daftar Sekolah
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                :
                                <div className="action" onClick={() => this.props.method.goToPage('login')}>
                                    <i className="fas fa-chevron-left"></i>
                                    Login
                                </div>
                        }
                    </div>

                    {/* RIGHT CONTAINER */}
                    <div className="col-6 right-container">
                        <img src={LOGO.KEMKOMINFO_WHITE} className="logo float-right" />
                        <div className="accent">
                            <h1>
                                SMK <br />
                                &nbsp;&nbsp;CODING
                            </h1>
                        </div>
                        <p>Copyright © 2018 SMK Coding</p>
                    </div>

                </div>

                {/* 
                    SWITCH BETWEEN LOGIN FORM AND REGISTER FORM
                */}
                {
                    this.props.state.pagePosition === 'login'
                        ?
                        this.renderLogin()
                        :
                        this.props.state.pagePosition === 'register-school'
                            ?
                            this.renderRegisterSchool()
                            :
                            this.renderRegister()
                }

            </div>
        )
    }
}
