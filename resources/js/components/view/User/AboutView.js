import React, { Component } from 'react'

import PrimaryButton from '../components/button/PrimaryButton'
import { IMAGE, ICON } from '../../assets/Images'

export default class AboutView extends Component {
    render() {
        return (
            <div id="about">

                <div id="head">

                    <div className="container my-auto">

                        <div className="row no-gutter">

                            <div className="col-lg-7 col-md-6 col-12 left-container">
                                <div className='my-auto'>
                                    <h1>
                                        Kembangkan talenta <br />
                                        menuju Indonesia <br />
                                        <span>The Digital Energy of Asia.</span>
                                    </h1>
                                    <p>
                                        Melatih generasi muda indonesia dengan tema
                                        indonesia sinergi digital teknologi, kreatif
                                        inovatif bersama SMK CODING.
                                    </p>
                                    <div className="btn-container">
                                        <PrimaryButton title="Daftar Sekarang" stripe />
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-5 col-md-6 col-12 right-container">

                                <div className="row no-gutter align-items-center">

                                    <div className="col-6 left-pic">
                                        <img src={IMAGE.PIC1} className="head-pic extra-height" alt="" />
                                    </div>

                                    <div className="col-6 right-pic">
                                        <img src={IMAGE.PIC2} className="head-pic mb-4" alt="" />
                                        <img src={IMAGE.PIC3} className="head-pic" alt="" />
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div id="background">

                    <div className="container">

                        <div className="subtitle">
                            <h5>SMK CODING</h5>
                        </div>

                        <h1>
                            Kenapa gabung <span>SMKCODING</span> ?
                        </h1>

                        <div className="row no-gutter align-items-center">

                            <div className="col-lg-5 col-md-6 col-12">
                                <img src={ICON.HUGO_AI} className="ilustration" />
                            </div>

                            <div className="col-lg-7 col-md-6 col-12">
                                <p>
                                    Pesatnya laju perkembangan teknologi membuat industri di seluruh dunia kewalahan dalam mencari tenaga kerja berkemampuan IT yang memadai.
                                </p>
                                <p>
                                    Di Asia Tenggara, Indonesia adalah negara dengan jumlah pengusaha rintisan digital tertinggi.* Bahkan, pada 2020, jumlahnya berpeluang melonjak hingga <span>200.000</span> orang!
                                </p>
                            </div>

                            <div className="quote-container text-center mt-5">

                                <h1>
                                    <span className="quote-mark">“ &nbsp;</span>
                                    Faktanya, <span className="bold">lulusan SMK</span> yang seharusnya sudah <span className="bold">siap kerja</span>, <span className="bold">mendominasi jumlah pengangguran</span> terbuka.
                                    <span className="quote-mark">&nbsp; “</span>
                                </h1>

                            </div>

                        </div>

                    </div>

                </div>

                <div id="roadmap">

                    <h1>Roadmap Program</h1>

                    <div className="container-fluid">

                        <div className="row no-gutter">

                            <div className="col-md-3 col-12 item">
                                <h2>Kuartial I</h2>

                                <h4>Sekolah Akhir Pekan</h4>
                                <p>Android Java</p>
                                <p>IoT</p>
                                <br />
                                <h4>Sekolah Intensif</h4>
                                <p>ASP.Net</p>
                                <p>Preparation</p>
                                <p>Trainisng for Trainers</p>
                            </div>

                            <div className="col-md-3 col-12 item">
                                <h2>Kuartial II</h2>

                                <h4>Sekolah Intensif</h4>
                                <p>Web App</p>
                                <p>Mobile App</p>
                                <br />
                                <h4>Sekolah Intensif</h4>
                                <p>Android Kotlin</p>
                                <p>Azure Cloud Platform</p>
                                <p>Bursa Kerja</p>
                            </div>

                            <div className="col-md-3 col-12 item">
                                <h2>Kuartial III</h2>

                                <h4>Sekolah Intensif</h4>
                                <p>Web App</p>
                                <p>Mobile App</p>
                                <br />
                                <h4>Sekolah Intensif</h4>
                                <p>Android Kotlin</p>
                                <p>Azure Cloud Platform</p>
                                <p>Bursa Kerja</p>
                            </div>

                            <div className="col-md-3 col-12 item">
                                <h2>Kuartial III</h2>

                                <h4>Sekolah Intensif</h4>
                                <p>Web App</p>
                                <p>Mobile App</p>
                                <br />
                                <h4>Sekolah Intensif</h4>
                                <p>Android Kotlin</p>
                                <p>Azure Cloud Platform</p>
                                <p>Bursa Kerja</p>
                            </div>

                        </div>

                        <div className="timeline">
                            <div className="circle"></div>
                            <div className="line"></div>
                            <div className="circle"></div>
                            <div className="line"></div>
                            <div className="circle"></div>
                            <div className="line"></div>
                            <div className="circle"></div>
                        </div>

                    </div>

                </div>

            </div>
        )
    }
}
