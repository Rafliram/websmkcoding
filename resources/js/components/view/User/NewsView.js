import React, { Component } from 'react'
import NewsCard from '../components/card/NewsCard'
import NewsHorizontal from '../components/card/NewsHorizontal'
import PrimarySearch from '../components/form/PrimarySearch'

export default class NewsView extends Component {
    render() {
        return (
            <div id="news">

                <div id="head">

                    <div className="container">

                        <div className="row">

                            <div className="col-lg-9 col-md-12 col-12 pos-relative ">

                                <img src="https://images.unsplash.com/photo-1523580494863-6f3031224c94?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60" className="hero-pic" />

                                <div className="desc-container box-shadow">
                                    <h1>Event pengenalan dan pelatihan smk coding Malang.</h1>

                                    <p className="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit . . . </p>

                                    <h6>READ MORE</h6>

                                    <p className="date"> 25 Maret 2020</p>
                                </div>

                            </div>

                            <div className="col-lg-3 col-md-12 col-12 mt-lg-0 mt-4">

                                <div className="row">

                                    <div className="col-lg-12 col-md-6 col-12">
                                        <div className="item-wrapper">
                                            <img src="https://images.unsplash.com/photo-1505373877841-8d25f7d46678?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60" className="pic" />
                                            <div className="mask">
                                                <h3 className="title">CHALLENGE II SMK Coding Start!!</h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-12 col-md-6 col-12">
                                        <div className="item-wrapper">
                                            <img src="https://images.unsplash.com/photo-1560523160-754a9e25c68f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60" className="pic" />
                                            <div className="mask">
                                                <h3 className="title">Workshop how to bussiness SMK Malang</h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div id="content">
                    <div className="container">

                        <div className="row">

                            {/* CONTENT */}
                            <div className="col-lg-9 col-md-12 col-12">

                                <div className="row">

                                    <div className="col-12 mb-4">
                                        <PrimarySearch
                                            name="search"
                                            onChange={(e) => console.log(e)} />
                                    </div>

                                    <div className="col-md-4 col-12 item-wrapper">

                                        <NewsCard
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1527822618093-743f3e57977c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                            title="Seleksi smk coding gelombang 1"
                                            date="25 maret 2020" />

                                    </div>

                                    <div className="col-md-4 col-12 item-wrapper">

                                        <NewsCard
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1527822618093-743f3e57977c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                            title="Seleksi smk coding gelombang 1"
                                            date="25 maret 2020" />

                                    </div>

                                    <div className="col-md-4 col-12 item-wrapper">

                                        <NewsCard
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1527822618093-743f3e57977c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                            title="Seleksi smk coding gelombang 1"
                                            date="25 maret 2020" />

                                    </div>

                                    <div className="col-md-4 col-12 item-wrapper">

                                        <NewsCard
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1527822618093-743f3e57977c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                            title="Seleksi smk coding gelombang 1"
                                            date="25 maret 2020" />

                                    </div>

                                    <div className="col-md-4 col-12 item-wrapper">

                                        <NewsCard
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1527822618093-743f3e57977c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                            title="Seleksi smk coding gelombang 1"
                                            date="25 maret 2020" />

                                    </div>

                                    <div className="col-md-4 col-12 item-wrapper">

                                        <NewsCard
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1527822618093-743f3e57977c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                            title="Seleksi smk coding gelombang 1"
                                            date="25 maret 2020" />

                                    </div>

                                    <div className="col-lg-6 col-md-6 col-12">
                                        <NewsHorizontal
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1512758017271-d7b84c2113f1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80"
                                            title="Workshop mind mapping bussines plan" />
                                    </div>

                                    <div className="col-lg-6 col-md-6 col-12 mt-lg-0 mt-5">
                                        <NewsHorizontal
                                            taq="new generation"
                                            image="https://images.unsplash.com/photo-1559223607-a43c990c692c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60"
                                            title="Training calon mentor 2020" />
                                    </div>

                                </div>

                            </div>

                            {/* Side News */}
                            <div className="col-lg-3 col-md-12 col-12 mt-lg-0 mt-5">

                                <div className="side">
                                    <h5>Kabar Terbaru</h5>

                                    <div className="item">
                                        <h4>Event pengenalan dan pelatihan smk coding Malang.</h4>
                                        <p className="date">25 maret 2020</p>
                                    </div>

                                    <div className="item">
                                        <h4>PEMENANG QUIZ COMPETITION SE-KOTA MALANG 2020</h4>
                                        <p className="date">25 maret 2020</p>
                                    </div>

                                    <div className="item">
                                        <h4>Workshop how to bussiness SMK Malang</h4>
                                        <p className="date">25 maret 2020</p>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        )
    }
}
