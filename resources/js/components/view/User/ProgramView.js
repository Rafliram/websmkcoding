import React, { Component } from 'react'
import { ICON, IMAGE } from '../../assets/Images'

export default class ProgramView extends Component {
    render() {
        return (
            <div id="program-page">

                <div className="container">

                    {/* Program I */}
                    <div className="program-section">

                        <div className="row my-auto w-100 align-items-center">

                            {/* DESC */}
                            <div className="col-lg-10 col-md-8 col-12">

                                <div className="desc">
                                    <h5>PROGRAM I</h5>

                                    <h1>Sekolah Intensif</h1>

                                    <div className="line"></div>
                                    <p>
                                        Merupakan program pelatihan gratis
                                        Bersertifikasi yang diakui dan sesuai dengan standar Kementrian Komunikasi dan Informatika Republik Indonesia Kelas pelatihan dengan berbasis Project Based Learning dan kurikulum yang
                                        relevan dan efektif yang telah disesuaikan dengan kebutuhan Industri.
                                    <br />
                                        <br />
                                        Terdapat 10 kali pertemuan setiap Sabtu & Minggu selam 5 pekan
                                        On-site Trainer dari komunitas (Indonesia Android Network dan Microsoft User
                                        Group Indonesia)
                                    </p>
                                </div>

                            </div>
                            
                            {/* MATERI */}
                            <div className="col-lg-2 col-md-4 col-12">
                                <div className="row no-gutter">
                                    <div className='col-6'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.DATASCIENCE_POLYGON} className="icon-materi" />
                                    </div>
                                    <div className='col-6 pt-5'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb" />
                                    </div>
                                </div>
                            </div>

                        </div>

                        {/* ACCENT */}
                        <div className="polygon-accent">
                            <img src={IMAGE.POLYGON} className="background-object" />
                        </div>

                    </div>

                    {/* Program II */}
                    <div className="program-section">

                        <div className="row my-auto w-100 align-items-center">

                            {/* MATERI */}
                            <div className="col-lg-2 col-md-4 col-12">
                                <div className="row no-gutter">
                                    <div className='col-6'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.DATASCIENCE_POLYGON} className="icon-materi" />
                                    </div>
                                    <div className='col-6 pt-5'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb" />
                                    </div>
                                </div>
                            </div>

                            {/* DESC */}
                            <div className="col-lg-10 col-md-8 col-12 pl-5">

                                <div className="desc pl-3">
                                    <h5>PROGRAM II</h5>

                                    <h1>Sekolah Intensif</h1>

                                    <div className="line"></div>
                                    <p>
                                        Merupakan program pelatihan gratis
                                        Bersertifikasi yang diakui dan sesuai dengan standar Kementrian Komunikasi dan Informatika Republik Indonesia Kelas pelatihan dengan berbasis Project Based Learning dan kurikulum yang
                                        relevan dan efektif yang telah disesuaikan dengan kebutuhan Industri.
                                    <br />
                                        <br />
                                        Terdapat 10 kali pertemuan setiap Sabtu & Minggu selam 5 pekan
                                        On-site Trainer dari komunitas (Indonesia Android Network dan Microsoft User
                                        Group Indonesia)
                                    </p>
                                </div>

                            </div>

                        </div>

                        {/* ACCENT */}
                        <div className="circle-accent"></div>
                        <div className="circle-accent"></div>

                    </div>

                    {/* Program III */}
                    <div className="program-section">

                        <div className="row my-auto w-100 align-items-center">

                            {/* DESC */}
                            <div className="col-lg-10 col-md-8 col-12">

                                <div className="desc">
                                    <h5>PROGRAM III</h5>

                                    <h1>Sekolah Intensif</h1>

                                    <div className="line"></div>
                                    <p>
                                        Merupakan program pelatihan gratis
                                        Bersertifikasi yang diakui dan sesuai dengan standar Kementrian Komunikasi dan Informatika Republik Indonesia Kelas pelatihan dengan berbasis Project Based Learning dan kurikulum yang
                                        relevan dan efektif yang telah disesuaikan dengan kebutuhan Industri.
                                        <br />
                                        <br />
                                        Terdapat 10 kali pertemuan setiap Sabtu & Minggu selam 5 pekan
                                        On-site Trainer dari komunitas (Indonesia Android Network dan Microsoft User
                                        Group Indonesia)
                                    </p>
                                </div>

                            </div>
                            {/* MATERI */}
                            <div className="col-lg-2 col-md-4 col-12">
                                <div className="row no-gutter">
                                    <div className='col-6'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.DATASCIENCE_POLYGON} className="icon-materi" />
                                    </div>
                                    <div className='col-6 pt-5'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb" />
                                    </div>
                                </div>
                            </div>

                        </div>

                        {/* ACCENT */}
                        <div className="square-accent"></div>

                    </div>
                    
                    {/* Program II */}
                    <div className="program-section">

                        <div className="row my-auto w-100 align-items-center">

                            {/* MATERI */}
                            <div className="col-lg-2 col-md-4 col-12">
                                <div className="row no-gutter">
                                    <div className='col-6'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.DATASCIENCE_POLYGON} className="icon-materi" />
                                    </div>
                                    <div className='col-6 pt-5'>
                                        <img src={ICON.ANDROID_POLYGON} className="icon-materi mb-4" />
                                        <img src={ICON.AZURE_POLYGON} className="icon-materi mb" />
                                    </div>
                                </div>
                            </div>

                            {/* DESC */}
                            <div className="col-lg-10 col-md-8 col-12 pl-5">

                                <div className="desc pl-3">
                                    <h5>PROGRAM IV</h5>

                                    <h1>Sekolah Intensif</h1>

                                    <div className="line"></div>
                                    <p>
                                        Merupakan program pelatihan gratis
                                        Bersertifikasi yang diakui dan sesuai dengan standar Kementrian Komunikasi dan Informatika Republik Indonesia Kelas pelatihan dengan berbasis Project Based Learning dan kurikulum yang
                                        relevan dan efektif yang telah disesuaikan dengan kebutuhan Industri.
                                    <br />
                                        <br />
                                        Terdapat 10 kali pertemuan setiap Sabtu & Minggu selam 5 pekan
                                        On-site Trainer dari komunitas (Indonesia Android Network dan Microsoft User
                                        Group Indonesia)
                                    </p>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        )
    }
}
