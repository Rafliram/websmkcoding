import React, { Component } from 'react'

import { css } from "@emotion/core";
import BeatLoader from "react-spinners/BeatLoader";

const override = css`
  display: block;
  margin: 0 auto;
`;

export default class LoadingSpinner extends Component {
    render() {
        return (
            <div className="sweet-loading">
                <BeatLoader
                    css={override}
                    size={this.props.size === undefined ? 12 : this.props.size}
                    color={this.props.color === undefined ? '#ffffff' : this.props.color}
                    loading={this.props.loading}
                />
            </div>
        )
    }
}
