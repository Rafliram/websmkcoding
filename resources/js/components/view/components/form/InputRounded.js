import React, { Component } from 'react'

export default class InputRounded extends Component {

    constructor(props) {

        super(props)

        this.state = {
            hidden: true
        }

    }

    render() {
        return (
            this.props.type !== undefined
                ?
                // IF NOT UNDEFINED
                <div id="input-rounded-password">
                    <input
                        type={this.state.hidden === true ? 'password' : 'text'}
                        value={this.props.value}
                        name={this.props.name === undefined ? "name" : this.props.name}
                        placeholder={this.props.placeholder === undefined ? "" : this.props.placeholder}
                        onChange={(e) => this.props.onChange(e)} />
                    {
                        this.state.hidden === true
                            ?
                                <i className="icon fas fa-eye" onClick={() => this.setState({ hidden: !this.state.hidden })} />
                            :
                                <i className="icon fas fa-eye-slash" onClick={() => this.setState({ hidden: !this.state.hidden })} />
                        }
                </div>
                :
                // IF UNEFINED
                <div id="input-rounded">
                    <input
                        type={this.props.type === undefined ? "text" : this.props.type}
                        value={this.props.value}
                        name={this.props.name === undefined ? "name" : this.props.name}
                        placeholder={this.props.placeholder === undefined ? "" : this.props.placeholder}
                        onChange={(e) => this.props.onChange(e)} />
                </div>
        )
    }
}
