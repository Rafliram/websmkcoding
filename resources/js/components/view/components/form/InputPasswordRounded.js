import React, { Component } from 'react'

export default class InputPasswordRounded extends Component {
    render() {
        return (
            <div>
                <input
                    id="input-rounded"
                    type={this.props.type === undefined ? "text" : this.props.type}
                    value={this.props.value}
                    name={this.props.name === undefined ? "name" : this.props.name}
                    placeholder={this.props.placeholder === undefined ? "" : this.props.placeholder}
                    onChange={(e) => this.props.onChange(e)} /> 
            </div>
        )
    }
}
