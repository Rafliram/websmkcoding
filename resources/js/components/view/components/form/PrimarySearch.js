import React, { Component } from 'react'

export default class PrimarySearch extends Component {
    render() {
        return (
            <div id="primary-search">
                <i class="fas fa-search">
                    <input
                    className="main-input"
                    name={this.props.name}
                    type="text"
                    placeholder={this.props.placeholder === undefined ? "Search" : this.props.placeholder}
                    onChange={(e)=>this.props.onChange(e)}/>
                </i>
            </div>
        )
    }
}
