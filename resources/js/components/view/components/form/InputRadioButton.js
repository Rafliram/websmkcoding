import React, { Component } from 'react'

export default class InputRadioButton extends Component {
    render() {
        return (
            <input
                defaultChecked = {this.props.defaultChecked === undefined ? false : true}
                type="radio"
                name={this.props.name}
                value={this.props.value}
                onChange={(e) => this.props.onChange(e)} />
        )
    }
}
