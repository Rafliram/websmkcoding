import React, { Component } from 'react'

export default class RoundedLabel extends Component {
    render() {
        return (
            <div id="rounded-label">
                {this.props.title === undefined ? "---" : this.props.title}
            </div>
        )
    }
}
