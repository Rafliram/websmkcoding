import React, { Component } from 'react'
import { LOGO } from '../../assets/Images'

export default class Separator extends Component {
    render() {
        return (
            <div id="separator">
                <h5><img src={LOGO.LOGO_NOCAPTION} className="logo"/></h5>
            </div>
        )
    }
}
