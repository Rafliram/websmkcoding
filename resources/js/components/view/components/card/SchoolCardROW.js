import React, { Component } from 'react'
import { IMAGE } from '../../../assets/Images'

export default class SchoolCardROW extends Component {
    render() {
        return (
            <div id="school-card" className={this.props.col}>

                <img src={this.props.image} className="item-pic" />

                <div className="overlay">

                    <div className="top">
                        <h4>{this.props.schoolName}</h4>
                        <span>
                            {this.props.headmaster}
                    </span>
                    </div>

                    <div className="bottom">

                        <span> {this.props.location} </span>

                        <br />

                        <button className="address">{this.props.address}</button>

                    </div>

                </div>

            </div>
        )
    }
}
