import React, { Component } from 'react'

export default class NewsHorizontal extends Component {
    render() {
        return (
            <div id="news-horizontal">
                <img src={this.props.image} />

                <div className="overlay">

                    <div className="label">
                        {this.props.taq === undefined ? "taq" : this.props.taq}
                    </div>

                    <h4>{this.props.title === undefined ? "Title" : this.props.title}</h4>

                    <p>{this.props.date === undefined ? "date" : this.props.date}</p>

                </div>

            </div>
        )
    }
}
