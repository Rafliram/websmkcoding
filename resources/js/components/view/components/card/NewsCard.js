import React, { Component } from 'react'

export default class NewsCard extends Component {
    render() {
        console.log(this.props.date)
        return (
            <div id="NewsCard" className="thin-shadow">
                <img src={this.props.image} className="pic"/>

                <div className="desc">
                    <div className="label">{this.props.tag ? "taq" : this.props.taq}</div>

                    <h1>{this.props.title === undefined ? "title" : this.props.title}</h1>

                    <p>{this.props.date === undefined ? "date" : this.props.date}</p>

                </div>
            </div>
        )
    }
}
