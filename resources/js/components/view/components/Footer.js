import React, { Component } from 'react'
import { LOGO } from '../../assets/Images'

export default class Footer extends Component {
    render() {
        return (
            <div id="footer">

                <div className="container text-center">

                    <img src={LOGO.KEMKOMINFO_WHITE} className="logo" />

                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a class="social-icon text-xs-center active" target="_blank">Beranda</a>
                        </li>
                        <li class="list-inline-item">
                            <a class="social-icon text-xs-center" target="_blank">Tentang</a>
                        </li>
                        <li class="list-inline-item">
                            <a class="social-icon text-xs-center" target="_blank">Mentor</a>
                        </li>
                        <li class="list-inline-item">
                            <a class="social-icon text-xs-center" target="_blank">Program</a>
                        </li>
                        <li class="list-inline-item">
                            <a class="social-icon text-xs-center" target="_blank">SMK Terdaftar</a>
                        </li>
                    </ul>

                    <div className="stripe"></div>

                    <p>Copyright © 2018 SMK Coding</p>

                </div>

            </div>
        )
    }
}
