import React, { Component } from 'react'

import { Link } from 'react-router-dom';

import { LOGO } from '../../assets/Images'

export default class Navbar extends Component {


    render() {

        const url = window.location.pathname
        var exact_path = url.substr(1, url.length)

        return (
            <div id="header-navbar">

                <nav className="navbar navbar-expand-lg fixed">

                    <div className="container">

                        <img src={LOGO.LOGO_HORIZONTAL} className="logo" />
                        {/* <a className="navbar-brand" href="/">
                                as
                            </a> */}

                        <button className="navbar-toggler custom-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse custom-navbar-collapse" id="navbarSupportedContent">

                            <ul className="navbar-nav ml-auto my-1 mr-0 justify-content-center align-items-center" id="link-container">
                                <li className={`nav-item ${exact_path === '' ? "active" : ""}`}>
                                    <Link className={this.props.light === undefined ? "" : "light-theme"} to="/">Beranda</Link>
                                </li>
                                <li className={`nav-item ${exact_path === 'about' ? "active" : ""}`}>
                                    <Link className={this.props.light === undefined ? "" : "light-theme"} to="/about">Tentang</Link>
                                </li>
                                <li className={`nav-item ${exact_path === 'mentor' ? "active" : ""}`}>
                                    <a href="/mentor" className={this.props.light === undefined ? "" : "light-theme"}>Mentor</a>
                                    {/* <Link className={this.props.light === undefined ? "" : "light-theme"} to="/mentor">Mentor</Link> */}
                                </li>
                                <li className={`nav-item ${exact_path === 'program' ? "active" : ""}`}>
                                    <Link className={this.props.light === undefined ? "" : "light-theme"} to="/program">Program</Link>
                                </li>
                                <li className={`nav-item ${exact_path === 'news' ? "active" : ""}`}>
                                    <Link className={this.props.light === undefined ? "" : "light-theme"} to="/news">News</Link>
                                </li>
                                <li className={`nav-item ${exact_path === 'smkterdaftar' ? "active" : ""}`}>
                                    <Link className={this.props.light === undefined ? "" : "light-theme"} to="/smkterdaftar">SMK Terdaftar</Link>
                                </li>
                                <li className="nav-item mr-0">
                                    <div className="btn-group">
                                        <button type="button" className="btn register-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Daftar
                                        </button>
                                        <div className="dropdown-menu">
                                            <Link to="/auth/register" className="dropdown-item">
                                                <i className="fas fa-user-graduate"></i> Daftar Siswa
                                            </Link>
                                            <Link to="/auth/register-school" className="dropdown-item">
                                                <i className="fas fa-school"></i> Daftar Sekolah
                                            </Link>
                                            <div className="dropdown-divider"></div>
                                            <Link to="/auth/login" className="dropdown-item">
                                                <i className="fas fa-user"></i> Login
                                            </Link>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                        </div>

                    </div>

                </nav>

            </div>

        )
    }
}
