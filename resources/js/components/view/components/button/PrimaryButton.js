import React, { Component } from 'react'
import LoadingSpinner from '../LoadingSpinner'

export default class PrimaryButton extends Component {
    render() {
        return (
            <div className="w-100" id={this.props.stripe === undefined ? "primary-button-nostripe" : "primary-button"}>

                <button
                    style={{ width: '100%' }}
                    onClick={() => this.props.onClick()}
                    className={this.props.light === undefined ? "blue" : "light"}>
                    {
                        this.props.withLoading === undefined
                            ?
                            this.props.title === undefined ? "Oups!" : this.props.title
                            :
                            this.props.loading === true
                                ?
                                <LoadingSpinner color="#ffffff"/>
                                :
                                this.props.title === undefined ? "Oups!" : this.props.title
                    }
                </button>

            </div>
        )
    }
}
