<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'isadmin'], function () {
    // News
    Route::post('news/create', 'Api\News\NewsController@create');
    Route::post('news/update', 'Api\News\NewsController@update');
    Route::post('news/delete', 'Api\News\NewsController@delete');
});

// Admins
Route::get('admins', 'Api\Admin\AdminController@admins');
Route::post('admin/login', 'Api\Admin\AdminController@login');
Route::post('admin/register', 'Api\Admin\AdminController@register');
Route::post('admin/delete', 'Api\Admin\AdminController@delete');
Route::post('admin/update', 'Api\Admin\AdminController@update');
// Mentor
Route::get('mentor', 'Api\Mentor\MentorController@mentor');
Route::post('mentor/create', 'Api\Mentor\MentorController@create');
Route::post('mentor/update', 'Api\Mentor\MentorController@update');
Route::post('mentor/delete', 'Api\Mentor\MentorController@delete');
// Users
Route::get('users', 'Api\User\UserController@users');
Route::post('user/login', 'Api\User\UserController@login');
Route::post('user/register', 'Api\User\UserController@register');
Route::post('user/delete', 'Api\User\UserController@delete');
Route::post('user/update', 'Api\User\UserController@update');
// Sekolah
Route::get('sekolah', 'Api\Sekolah\SekolahController@sekolah');
Route::post('sekolah/register', 'Api\Sekolah\SekolahController@register');
Route::post('sekolah/update', 'Api\Sekolah\SekolahController@update');
Route::post('sekolah/delete', 'Api\Sekolah\SekolahController@delete');
// News
Route::get('news', 'Api\News\NewsController@news');
// Provinsi Indonesia
Route::get('provinsi', 'Api\ProvinsiIndonesia\ProvinsiIndonesiaController@provinsi');
Route::post('provinsi/create', 'Api\ProvinsiIndonesia\ProvinsiIndonesiaController@create');
Route::post('provinsi/update', 'Api\ProvinsiIndonesia\ProvinsiIndonesiaController@update');
Route::post('provinsi/delete', 'Api\ProvinsiIndonesia\ProvinsiIndonesiaController@delete');
// Kota Indonesia
Route::get('kota', 'Api\KotaIndonesia\KotaIndonesiaController@kota');
Route::post('kota/create', 'Api\KotaIndonesia\KotaIndonesiaController@create');
Route::post('kota/update', 'Api\KotaIndonesia\KotaIndonesiaController@update');
Route::post('kota/delete', 'Api\KotaIndonesia\KotaIndonesiaController@delete');
// Kategori kegiatan
Route::get('kategori', 'Api\Kategori\KategoriKegiatanController@kategori');
Route::post('kategori/create', 'Api\Kategori\KategoriKegiatanController@create');
Route::post('kategori/update', 'Api\Kategori\KategoriKegiatanController@update');
Route::post('kategori/delete', 'Api\Kategori\KategoriKegiatanController@delete');
// Gallery
Route::get('gallery', 'Api\Page\Gallery\GalleryController@gallery');
Route::post('gallery/create', 'Api\Page\Gallery\GalleryController@create');
Route::post('gallery/update', 'Api\Page\Gallery\GalleryController@update');
Route::post('gallery/delete', 'Api\Page\Gallery\GalleryController@delete');
// Page Header
Route::get('header', 'Api\Page\Header\HeaderController@header');
Route::post('header/create', 'Api\Page\Header\HeaderController@create');
Route::post('header/update', 'Api\Page\Header\HeaderController@update');
Route::post('header/delete', 'Api\Page\Header\HeaderController@delete');
// Page About
Route::get('about', 'Api\Page\About\AboutController@about');
Route::post('about/create', 'Api\Page\About\AboutController@create');
Route::post('about/update', 'Api\Page\About\AboutController@update');
Route::post('about/delete', 'Api\Page\About\AboutController@delete');
// Page Pembekalan
Route::get('pembekalan', 'Api\Page\Pembekalan\PembekalanController@pembekalan');
Route::post('pembekalan/create', 'Api\Page\Pembekalan\PembekalanController@create');
Route::post('pembekalan/update', 'Api\Page\Pembekalan\PembekalanController@update');
Route::post('pembekalan/delete', 'Api\Page\Pembekalan\PembekalanController@delete');
// Page Materi
Route::get('materi', 'Api\Page\Materi\MateriController@materi');
Route::post('materi/create', 'Api\Page\Materi\MateriController@create');
Route::post('materi/update', 'Api\Page\Materi\MateriController@update');
Route::post('materi/delete', 'Api\Page\Materi\MateriController@delete');
// Page Footer
Route::get('footer', 'Api\Page\Footer\FooterController@footer');
Route::post('footer/create', 'Api\Page\Footer\FooterController@create');
Route::post('footer/update', 'Api\Page\Footer\FooterController@update');
Route::post('footer/delete', 'Api\Page\Footer\FooterController@delete');
// Page Program
Route::get('program', 'Api\Page\Program\ProgramController@program');
Route::post('program/create', 'Api\Page\Program\ProgramController@create');
Route::post('program/update', 'Api\Page\Program\ProgramController@update');
Route::post('program/delete', 'Api\Page\Program\ProgramController@delete');
// Page Benefit
Route::get('benefit', 'Api\Page\Benefit\BenefitController@benefit');
Route::post('benefit/create', 'Api\Page\Benefit\BenefitController@create');
Route::post('benefit/update', 'Api\Page\Benefit\BenefitController@update');
Route::post('benefit/delete', 'Api\Page\Benefit\BenefitController@delete');
// Batch Pelatihan
Route::get('pelatihan', 'Api\Batch\Pelatihan\PelatihanController@batchPelatihan');
Route::post('pelatihan/create', 'Api\Batch\Pelatihan\PelatihanController@create');
Route::post('pelatihan/update', 'Api\Batch\Pelatihan\PelatihanController@update');
Route::post('pelatihan/delete', 'Api\Batch\Pelatihan\PelatihanController@delete');
// Batch Pelatihan Sekolah
Route::get('pelatihanSekolah', 'Api\Batch\PelatihanSekolah\PelatihanSekolahController@batchPelatihanSekolah');
Route::post('pelatihanSekolah/create', 'Api\Batch\PelatihanSekolah\PelatihanSekolahController@create');
Route::post('pelatihanSekolah/update', 'Api\Batch\PelatihanSekolah\PelatihanSekolahController@update');
Route::post('pelatihanSekolah/delete', 'Api\Batch\PelatihanSekolah\PelatihanSekolahController@delete');