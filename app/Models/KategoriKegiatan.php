<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriKegiatan extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'kategori_kegiatan';
}
