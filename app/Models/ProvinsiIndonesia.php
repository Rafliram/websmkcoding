<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProvinsiIndonesia extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'provinsi_indonesia';
}
