<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'page_footer';
}
