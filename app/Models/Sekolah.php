<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'sekolah';
}
