<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mentor extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'mentor';
}
