<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BatchPelatihanSekolah extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'batch_pelatihan_sekolah';
}
