<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageAbout extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'page_about';
}
