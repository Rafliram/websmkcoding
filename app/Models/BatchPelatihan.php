<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BatchPelatihan extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $table = 'batch_pelatihan';
}
