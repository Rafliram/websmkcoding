<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $permohonan = "permohonan";
    public $news = "news";
    public $images = "images";
    public $BASE_URL = "http://ec2-18-141-160-80.ap-southeast-1.compute.amazonaws.com/my-app1/public/";
//    public $BASE_URL = "http://localhost/smkcoding/public/";
//    public $BASE_URL = "http://127.0.0.1/websmkcoding/public/";

    protected function responseSuccess($message, $data)
    {
        return response()->json([
            "status" => true,
            "message" => $message,
            "data" => $data
        ], 200);
    }

    protected function responseError($error, $message)
    {
        return response()->json([
            "status" => false,
            "error" => $error,
            "message" => $message
        ], 400);
    }

    public function urlFile($filename, $type)
    {
        if ($type == $this->permohonan) {
            return $this->BASE_URL."permohonan/" . $filename;
        } else if ($type == $this->news) {
            return $this->BASE_URL."images/news/" . $filename;
        } else if($type == $this->images) {
            return $this->BASE_URL."images/" . $filename;
        }
    }
    public function dateNow(){

        date_default_timezone_set("Asia/Jakarta");
        return date('Y-m-d h:i:s');
    }
}
