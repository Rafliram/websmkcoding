<?php

namespace App\Http\Controllers\Api\Page\Program;

use App\Http\Controllers\Api\Controller;
use App\Models\Program;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * All Program
     */
    public function program()
    {
        $data = Program::orderBy('title')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Program
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'materi' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }


        // Proses input data
        $req = $request->all();
        $footer = new Program();

        foreach ($req as $key => $values) {
            $footer[$key] = $values;
        }

        date_default_timezone_set("Asia/Jakarta");
        $footer['createAt'] = $this->dateNow();
        $footer['updateAt'] = $this->dateNow();

        if ($footer->save()) {

            $data = Program::where('id', $footer['id'])->get();
            return $this->responseSuccess('Success create footer', $data);

        } else {
            return $this->responseError('Failed create footer', 'Failed create footer');
        }
    }
    /**
     * Update Program
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'materi' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        date_default_timezone_set("Asia/Jakarta");
        $update = Program::where('id', $request->id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'materi' => $request->materi,
            'updateAt' => $this->dateNow()
        ]);
        
        if ($update) {
            $data = Program::where('id', $request->id)->get();
            return $this->responseSuccess('Success update footer', $data);
        } else {
            return $this->responseError('Failed update footer', 'Nothing to update');
        }
    }
    /**
     * Delete Program
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = Program::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}
