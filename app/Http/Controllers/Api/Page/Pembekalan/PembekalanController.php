<?php

namespace App\Http\Controllers\Api\Page\Pembekalan;

use App\Http\Controllers\Api\Controller;
use App\Models\PagePembekalan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PembekalanController extends Controller
{
    /**
     * All Page Pembekalan
     */
    public function pembekalan()
    {
        $data = PagePembekalan::orderBy('superTitle')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Page Pembekalan
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'superTitle' => 'required',
            'image' => 'required|file',
            'title' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "PEMBEKALAN" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $req = $request->all();
                $pagePembekalan = new PagePembekalan();

                foreach ($req as $key => $values) {

                    $pagePembekalan[$key] = $values;
                    if ($key == "image") {
                        $pagePembekalan[$key] = $this->urlFile($filename, $this->images);
                    }
                }
                // Set DateTime now
                $pagePembekalan['createAt'] = $this->dateNow();
                $pagePembekalan['updateAt'] = $this->dateNow();

                if ($pagePembekalan->save()) {

                    $data = PagePembekalan::where('id', $pagePembekalan['id'])->get();
                    return $this->responseSuccess('Success create pagePembekalan', $data);
                } else {
                    return $this->responseError('Failed create pagePembekalan', 'Failed create pagePembekalan');
                }
            } else {
                return $this->responseError('Failed create pagePembekalan', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed create pagePembekalan", "Request image");
        }
    }
    /**
     * Update Page Pembekalan
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'superTitle' => 'required',
            'image' => 'required|file',
            'title' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "PEMBEKALAN" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $delOlderImage = PagePembekalan::where('id', $request->id)->get();
                if (count($delOlderImage) == 1) {

                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR .substr($delOlderImage[0]->image, -40);
                    $update = PagePembekalan::where('id', $request->id)->update([
                        'superTitle' => $request->superTitle,
                        'image' => $this->urlFile($filename, $this->images),
                        'title' => $request->title,
                        'updateAt' => $this->dateNow(),
                    ]);
                    if ($update) {
                        // Remove olderImage
                        unlink($filePath);
                        $data = PagePembekalan::where('id', $request->id)->get();
                        return $this->responseSuccess('Success update pagePembekalan', $data);
                    }
                } else {
                    return $this->responseError('Failed update pagePembekalan', 'Failed update pagePembekalan');
                }
            } else {
                return $this->responseError('Failed update pagePembekalan', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed update pagePembekalan", "Request image");
        }
    }
    /**
     * Delete Page Pembekalan
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $pagePembekalan = PagePembekalan::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . substr($pagePembekalan->image, -40);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = PagePembekalan::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
