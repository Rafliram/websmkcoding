<?php

namespace App\Http\Controllers\Api\Page\Materi;

use App\Http\Controllers\Api\Controller;
use App\Models\PageMateri;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MateriController extends Controller
{
    /**
     * All Page Materi
     */
    public function materi()
    {
        $data = PageMateri::orderBy('superJudul')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Page Materi
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'superJudul' => 'required',
            'tahun' => 'required',
            'judul' => 'required',
            'image' => 'required|file',
            'deskripsi' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "MATERI" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $req = $request->all();
                $pageMateri = new PageMateri();

                foreach ($req as $key => $values) {

                    $pageMateri[$key] = $values;
                    if ($key == "image") {
                        $pageMateri[$key] = $this->urlFile($filename, $this->images);
                    }
                }
                // Set DateTime now
                $pageMateri['createAt'] = $this->dateNow();
                $pageMateri['updateAt'] = $this->dateNow();

                if ($pageMateri->save()) {

                    $data = PageMateri::where('id', $pageMateri['id'])->get();
                    return $this->responseSuccess('Success create pageMateri', $data);
                } else {
                    return $this->responseError('Failed create pageMateri', 'Failed create pageMateri');
                }
            } else {
                return $this->responseError('Failed create pageMateri', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed create pageMateri", "Request image");
        }
    }
    /**
     * Update Page Materi
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'superJudul' => 'required',
            'tahun' => 'required',
            'judul' => 'required',
            'image' => 'required|file',
            'deskripsi' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "MATERI" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $delOlderImage = PageMateri::where('id', $request->id)->get();
                if (count($delOlderImage) == 1) {

                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR .substr($delOlderImage[0]->image, -36);
                    $update = PageMateri::where('id', $request->id)->update([
                        'superJudul' => $request->superJudul,
                        'tahun' => $request->tahun,
                        'judul' => $request->judul,
                        'image' => $this->urlFile($filename, $this->images),
                        'deskripsi' => $request->deskripsi,
                        'updateAt' => $this->dateNow(),
                    ]);
                    if ($update) {
                        // Remove olderImage
                        unlink($filePath);
                        $data = PageMateri::where('id', $request->id)->get();
                        return $this->responseSuccess('Success update pageMateri', $data);
                    }
                } else {
                    return $this->responseError('Failed update pageMateri', 'Failed update pageMateri');
                }
            } else {
                return $this->responseError('Failed update pageMateri', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed update pageMateri", "Request image");
        }
    }
    /**
     * Delete Page Materi
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $pageMateri = PageMateri::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . substr($pageMateri->image, -36);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = PageMateri::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
