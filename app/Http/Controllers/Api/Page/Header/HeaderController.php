<?php

namespace App\Http\Controllers\Api\Page\Header;

use App\Http\Controllers\Api\Controller;
use App\Models\PageHeader;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HeaderController extends Controller
{
    /**
     * All Page Header
     */
    public function header()
    {
        $data = PageHeader::orderBy('judul')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Page Header
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'judul' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|file',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "HEADER" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $req = $request->all();
                $pageHeader = new PageHeader();

                foreach ($req as $key => $values) {

                    $pageHeader[$key] = $values;
                    if ($key == "image") {
                        $pageHeader[$key] = $this->urlFile($filename, $this->images);
                    }
                }
                // Set DateTime now
                $pageHeader['createAt'] = $this->dateNow();
                $pageHeader['updateAt'] = $this->dateNow();

                if ($pageHeader->save()) {

                    $data = PageHeader::where('id', $pageHeader['id'])->get();
                    return $this->responseSuccess('Success create pageHeader', $data);
                } else {
                    return $this->responseError('Failed create pageHeader', 'Failed create pageHeader');
                }
            } else {
                return $this->responseError('Failed create pageHeader', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed create pageHeader", "Request image");
        }
    }
    /**
     * Update Page Header
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|file',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "HEADER" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $delOlderImage = PageHeader::where('id', $request->id)->get();
                if (count($delOlderImage) == 1) {

                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR .substr($delOlderImage[0]->image, -36);
                    $update = PageHeader::where('id', $request->id)->update([
                        'judul' => $request->judul,
                        'deskripsi' => $request->deskripsi,
                        'image' => $this->urlFile($filename, $this->images),
                        'updateAt' => $this->dateNow(),
                    ]);
                    if ($update) {
                        // Remove olderImage
                        unlink($filePath);
                        $data = PageHeader::where('id', $request->id)->get();
                        return $this->responseSuccess('Success update pageHeader', $data);
                    }
                } else {
                    return $this->responseError('Failed update pageHeader', 'Failed update pageHeader');
                }
            } else {
                return $this->responseError('Failed update pageHeader', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed update pageHeader", "Request image");
        }
    }
    /**
     * Delete Page Header
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $pageHeader = PageHeader::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . substr($pageHeader->image, -36);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = PageHeader::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
