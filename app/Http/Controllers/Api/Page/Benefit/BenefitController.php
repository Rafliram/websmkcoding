<?php

namespace App\Http\Controllers\Api\Page\Benefit;

use App\Http\Controllers\Api\Controller;
use App\Models\Benefit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
class BenefitController extends Controller
{
    /**
     * All Page Benefit
     */
    public function benefit()
    {
        $data = Benefit::get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Page Benefit
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'tagline' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|file',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "BENEFIT" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $req = $request->all();
                $pageBenefit = new Benefit();

                foreach ($req as $key => $values) {

                    $pageBenefit[$key] = $values;
                    if ($key == "image") {
                        $pageBenefit[$key] = $this->urlFile($filename, $this->images);
                    }
                }
                // Set DateTime now
                $pageBenefit['createAt'] = $this->dateNow();
                $pageBenefit['updateAt'] = $this->dateNow();

                if ($pageBenefit->save()) {

                    $data = Benefit::where('id', $pageBenefit['id'])->get();
                    return $this->responseSuccess('Success create pageBenefit', $data);
                } else {
                    return $this->responseError('Failed create pageBenefit', 'Failed create pageBenefit');
                }
            } else {
                return $this->responseError('Failed create pageBenefit', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed create pageBenefit", "Request image");
        }
    }
    /**
     * Update Page Benefit
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'tagline' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|file',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "BENEFIT" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $delOlderImage = Benefit::where('id', $request->id)->get();
                if (count($delOlderImage) == 1) {

                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR .substr($delOlderImage[0]->image, -37);
                    $update = Benefit::where('id', $request->id)->update([
                        'tagline' => $request->tagline,
                        'judul' => $request->judul,
                        'deskripsi' => $request->deskripsi,
                        'image' => $this->urlFile($filename, $this->images),
                        'updateAt' => $this->dateNow(),
                    ]);
                    if ($update) {
                        // Remove olderImage
                        unlink($filePath);
                        $data = Benefit::where('id', $request->id)->get();
                        return $this->responseSuccess('Success update pageBenefit', $data);
                    }
                } else {
                    return $this->responseError('Failed update pageBenefit', 'Failed update pageBenefit');
                }
            } else {
                return $this->responseError('Failed update pageBenefit', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed update pageBenefit", "Request image");
        }
    }
    /**
     * Delete Page Benefit
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $pageBenefit = Benefit::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . substr($pageBenefit->image, -37);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = Benefit::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
