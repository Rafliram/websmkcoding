<?php

namespace App\Http\Controllers\Api\Page\Footer;

use App\Http\Controllers\Api\Controller;
use App\Models\Footer;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class FooterController extends Controller
{
    /**
     * All Footer
     */
    public function footer()
    {
        $data = Footer::orderBy('judul')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Footer
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'tagline' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }


        // Proses input data
        $req = $request->all();
        $footer = new Footer();

        foreach ($req as $key => $values) {
            $footer[$key] = $values;
        }

        date_default_timezone_set("Asia/Jakarta");
        $footer['createAt'] = $this->dateNow();
        $footer['updateAt'] = $this->dateNow();

        if ($footer->save()) {

            $data = Footer::where('id', $footer['id'])->get();
            return $this->responseSuccess('Success create footer', $data);

        } else {
            return $this->responseError('Failed create footer', 'Failed create footer');
        }
    }
    /**
     * Update Footer
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'tagline' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        date_default_timezone_set("Asia/Jakarta");
        $update = Footer::where('id', $request->id)->update([
            'tagline' => $request->tagline,
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'updateAt' => $this->dateNow()
        ]);
        
        if ($update) {
            $data = Footer::where('id', $request->id)->get();
            return $this->responseSuccess('Success update footer', $data);
        } else {
            return $this->responseError('Failed update footer', 'Nothing to update');
        }
    }
    /**
     * Delete Footer
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = Footer::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}
