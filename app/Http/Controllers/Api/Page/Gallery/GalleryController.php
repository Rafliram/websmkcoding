<?php

namespace App\Http\Controllers\Api\Page\Gallery;

use App\Http\Controllers\Api\Controller;
use App\Models\PageGallery;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    /**
     * All Page Gallery
     */
    public function gallery()
    {
        $data = PageGallery::get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Page Gallery
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'image' => 'required|file',
            'kategoryId' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "GALLERY" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $req = $request->all();
                $pageGallery = new PageGallery();

                foreach ($req as $key => $values) {

                    $pageGallery[$key] = $values;
                    if ($key == "image") {
                        $pageGallery[$key] = $this->urlFile($filename, $this->images);
                    }
                }
                // Set DateTime now
                $pageGallery['createAt'] = $this->dateNow();
                $pageGallery['updateAt'] = $this->dateNow();

                if ($pageGallery->save()) {

                    $data = PageGallery::where('id', $pageGallery['id'])->get();
                    return $this->responseSuccess('Success create pageGallery', $data);
                } else {
                    return $this->responseError('Failed create pageGallery', 'Failed create pageGallery');
                }
            } else {
                return $this->responseError('Failed create pageGallery', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed create pageGallery", "Request image");
        }
    }
    /**
     * Update Page Gallery
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'image' => 'required|file',
            'kategoryId' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "GALLERY" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $delOlderImage = PageGallery::where('id', $request->id)->get();
                if (count($delOlderImage) == 1) {

                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR .substr($delOlderImage[0]->image, -37);
                    $update = PageGallery::where('id', $request->id)->update([
                        'image' => $this->urlFile($filename, $this->images),
                        'kategoryId' => $request->kategoryId,
                        'updateAt' => $this->dateNow(),
                    ]);
                    if ($update) {
                        // Remove olderImage
                        unlink($filePath);
                        $data = PageGallery::where('id', $request->id)->get();
                        return $this->responseSuccess('Success update pageGallery', $data);
                    }
                } else {
                    return $this->responseError('Failed update pageGallery', 'Failed update pageGallery');
                }
            } else {
                return $this->responseError('Failed update pageGallery', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed update pageGallery", "Request image");
        }
    }
    /**
     * Delete Page Gallery
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $pageGallery = PageGallery::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . substr($pageGallery->image, -37);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = PageGallery::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
