<?php

namespace App\Http\Controllers\Api\Page\About;

use App\Http\Controllers\Api\Controller;
use App\Models\PageAbout;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AboutController extends Controller
{
     /**
     * All Page About
     */
    public function about()
    {
        $data = PageAbout::orderBy('judul')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Page About
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'image' => 'required|file',
            'tagline' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "ABOUT" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $req = $request->all();
                $pageAbout = new PageAbout();

                foreach ($req as $key => $values) {

                    $pageAbout[$key] = $values;
                    if ($key == "image") {
                        $pageAbout[$key] = $this->urlFile($filename, $this->images);
                    }
                }
                // Set DateTime now
                $pageAbout['createAt'] = $this->dateNow();
                $pageAbout['updateAt'] = $this->dateNow();

                if ($pageAbout->save()) {

                    $data = PageAbout::where('id', $pageAbout['id'])->get();
                    return $this->responseSuccess('Success create pageAbout', $data);
                } else {
                    return $this->responseError('Failed create pageAbout', 'Failed create pageAbout');
                }
            } else {
                return $this->responseError('Failed create pageAbout', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed create pageAbout", "Request image");
        }
    }
    /**
     * Update Page About
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'image' => 'required|file',
            'tagline' => 'required',
            'judul' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('image')) {

            $fileUpload = $request->file('image');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "ABOUT" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/', $filename)) {

                $delOlderImage = PageAbout::where('id', $request->id)->get();
                if (count($delOlderImage) == 1) {

                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR .substr($delOlderImage[0]->image, -35);
                    $update = PageAbout::where('id', $request->id)->update([
                        'tagline' => $request->tagline,
                        'judul' => $request->judul,
                        'deskripsi' => $request->deskripsi,
                        'image' => $this->urlFile($filename, $this->images),
                        'updateAt' => $this->dateNow(),
                    ]);
                    if ($update) {
                        // Remove olderImage
                        unlink($filePath);
                        $data = PageAbout::where('id', $request->id)->get();
                        return $this->responseSuccess('Success update pageAbout', $data);
                    }
                } else {
                    return $this->responseError('Failed update pageAbout', 'Failed update pageAbout');
                }
            } else {
                return $this->responseError('Failed update pageAbout', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed update pageAbout", "Request image");
        }
    }
    /**
     * Delete Page About
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $pageAbout = PageAbout::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . substr($pageAbout->image, -35);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = PageAbout::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
