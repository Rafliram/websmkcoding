<?php

namespace App\Http\Controllers\Api\Kategori;

use App\Http\Controllers\Api\Controller;
use App\Models\KategoriKegiatan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class KategoriKegiatanController extends Controller
{
    /**
     * All Kategori Kegiatan
     */
    public function kategori()
    {
        $data = KategoriKegiatan::orderBy('nama')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Kategori Kegiatan
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $req = $request->all();
        $kategoriKegiatan = new KategoriKegiatan();

        foreach ($req as $key => $values) {
            $kategoriKegiatan[$key] = $values;
        }

        if ($kategoriKegiatan->save()) {

            $data = KategoriKegiatan::where('id', $kategoriKegiatan['id'])->get();
            return $this->responseSuccess('Success create kategoriKegiatan', $data);
        } else {
            return $this->responseError('Failed create kategoriKegiatan', 'Failed create kategoriKegiatan');
        }
    }
    /**
     * Update Kategori Kegiatan
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'nama' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $update = KategoriKegiatan::where('id', $request->id)->update([
            'nama' => $request->nama,
        ]);
        if ($update) {
            $data = KategoriKegiatan::where('id', $request->id)->get();
            return $this->responseSuccess('Success update kategoriKegiatan', $data);
        } else {
            return $this->responseError('Failed update kategoriKegiatan', 'Nothing to update');
        }
    }
    /**
     * Delete Kategori Kegiatan
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = KategoriKegiatan::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}