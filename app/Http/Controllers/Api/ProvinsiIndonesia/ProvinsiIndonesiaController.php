<?php

namespace App\Http\Controllers\Api\ProvinsiIndonesia;

use App\Http\Controllers\Api\Controller;
use App\Models\ProvinsiIndonesia;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProvinsiIndonesiaController extends Controller
{
    /**
     * All Provinsi Indonesia
     */
    public function provinsi()
    {
        $data = ProvinsiIndonesia::orderBy('nama')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Provinsi Indonesia
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }


        // Proses input data
        $req = $request->all();
        $provinsi = new ProvinsiIndonesia();

        foreach ($req as $key => $values) {
            $provinsi[$key] = $values;
        }

        if ($provinsi->save()) {

            $data = ProvinsiIndonesia::where('id', $provinsi['id'])->get();
            return $this->responseSuccess('Success create provinsi', $data);
        } else {
            return $this->responseError('Failed create provinsi', 'Failed create provinsi');
        }
    }
    /**
     * Update Provinsi Indonesia
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'nama' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data

        $update = ProvinsiIndonesia::where('id', $request->id)->update([
            'nama' => $request->nama,
        ]);
        if ($update) {
            $data = ProvinsiIndonesia::where('id', $request->id)->get();
            return $this->responseSuccess('Success update provinsi', $data);
        } else {
            return $this->responseError('Failed update provinsi', 'Nothing to update');
        }
    }
    /**
     * Delete Provinsi Indonesia
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = ProvinsiIndonesia::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}
