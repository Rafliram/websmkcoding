<?php

namespace App\Http\Controllers\Api\KotaIndonesia;

use App\Http\Controllers\Api\Controller;
use App\Models\KotaIndonesia;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class KotaIndonesiaController extends Controller
{
    /**
     * All Kota Indonesia
     */
    public function kota()
    {
        $data = KotaIndonesia::orderBy('nama')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Kota Indonesia
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'provinsiId' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }


        // Proses input data
        $req = $request->all();
        $kota = new KotaIndonesia();

        foreach ($req as $key => $values) {
            $kota[$key] = $values;
        }

        if ($kota->save()) {

            $data = KotaIndonesia::where('id', $kota['id'])->get();
            return $this->responseSuccess('Success create kota', $data);

        } else {
            return $this->responseError('Failed create kota', 'Failed create kota');
        }
    }
    /**
     * Update Kota Indonesia
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'nama' => 'required',
            'provinsiId' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        $update = KotaIndonesia::where('id', $request->id)->update([
            'nama' => $request->nama,
            'provinsiId' => $request->provinsiId
        ]);
        
        if ($update) {
            $data = KotaIndonesia::where('id', $request->id)->get();
            return $this->responseSuccess('Success update kota', $data);
        } else {
            return $this->responseError('Failed update kota', 'Nothing to update');
        }
    }
    /**
     * Delete Kota Indonesia
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = KotaIndonesia::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}