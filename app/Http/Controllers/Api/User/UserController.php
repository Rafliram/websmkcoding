<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Api\Controller;
use App\Models\Admin;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Get aLl user
     */
    public function users()
    {
        $data = User::orderBy('nama')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }

    /**
     * login user
     */
    public function login(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Cek email
        $searchEmail = User::where('email', $request->email)->get();
        if (count($searchEmail) == 0) {
            return $this->responseError("Login failed", "Email not registered");
        }

        // Cek password
        if (!app('hash')->check($request->password, $searchEmail[0]->password)) {
            return $this->responseError("Login failed", "Wrong password");
        }

        // Proses login
        $token = Auth::guard('api')->attempt(['email' => $request->email, 'password' => $request->password]);

        $data = $searchEmail[0];
        $data['token'] = $token;

        return $this->responseSuccess("Login successful", $data);
    }
    /**
     * Register user
     */
    public function register(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'jenisKelamin' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirmPassword' => 'required',
            'noHp' => 'required',
            'sekolahId' => 'required',
            'kelas' => 'required',
            'lamaBelajarPemograman' => 'required',
            'belajarAndroid' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Cek email
        $searchEmailAdmin = Admin::where('email', $request->email)->get();
        $searchEmailUser = User::where('email', $request->email)->get();
        if (count($searchEmailAdmin) != 0 || count($searchEmailUser) != 0) {
            return $this->responseError("Register failed", "Email address already use");
        }

        // Password tidak sama
        if ($request->confirmPassword != $request->password) {
            return $this->responseError("Register failed", "Password not match");
        }

        // Proses register
        $input = $request->all();
        $input['password'] = bcrypt($input['confirmPassword']);
        // Set DateTime now
        $input['createAt'] = $this->dateNow();
        $input['updateAt'] = $this->dateNow();
        $user = User::create($input);
        if ($user) {
            
            $data = User::where('email', $request->email)->get()[0];
            // $data['token'] =  $user->createToken('MyApp')->accessToken;

            return $this->responseSuccess("Successfully registered", $data);
        } else {
            return $this->responseError("Register failed", "Register failed");
        }
    }
    /**
     * Update user
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'nama' => 'required',
            'noHp' => 'required',
            'sekolahId' => 'required',
            'kelas' => 'required',
            'lamaBelajarPemograman' => 'required',
            'belajarAndroid' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Check User
        if (count(User::where('id', $request->id)->get()) == 0) {
            return $this->responseError("Failed updated", "User not registered");
        }

        //Prosses Update
        date_default_timezone_set("Asia/Jakarta");
        $update = User::where('id', $request->id)->update([
            'nama' => $request->nama,
            'noHp' => $request->noHp,
            'sekolahId' => $request->sekolahId,
            'kelas' => $request->kelas,
            'lamaBelajarPemograman' => $request->lamaBelajarPemograman,
            'belajarAndroid' => $request->belajarAndroid,
            'updateAt' => $this->dateNow()
        ]);

        if ($update) {
            // Get Current Data
            $data = User::where('id', $request->id)->get()[0];
            return $this->responseSuccess("Successfully updated", $data);
        } else {
            return $this->responseError("Failed updated", "Nothing to update");
        }
    }
    /**
     * Delete user
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = User::where('id', $request->id)->delete();

        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}
