<?php

namespace App\Http\Controllers\Api\Mentor;

use App\Http\Controllers\Api\Controller;
use App\Models\Mentor;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MentorController extends Controller
{
    /**
     * All Mentor
     */
    public function mentor()
    {
        $data = Mentor::orderBy('nama')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Mentor
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required|email',
            'noHp' => 'required',
            'motto' => 'required',
            'spesialis' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        $req = $request->all();
        $mentor = new Mentor();

        foreach ($req as $key => $values) {
            $mentor[$key] = $values;
        }

        date_default_timezone_set("Asia/Jakarta");
        $mentor['createAt'] = $this->dateNow();
        $mentor['updateAt'] = $this->dateNow();

        if ($mentor->save()) {

            $data = Mentor::where('id', $mentor['id'])->get();
            return $this->responseSuccess('Success create mentor', $data);

        } else {
            return $this->responseError('Failed create mentor', 'Failed create mentor');
        }
    }
    /**
     * Update Mentor
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required|email',
            'noHp' => 'required',
            'motto' => 'required',
            'spesialis' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        
        date_default_timezone_set("Asia/Jakarta");
        $mentor['createAt'] = $this->dateNow();
        $mentor['updateAt'] = $this->dateNow();
        $update = Mentor::where('id', $request->id)->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'noHp' => $request->noHp,
            'motto' => $request->motto,
            'spesialis' => $request->spesialis,
            'createAt' => $this->dateNow()
        ]);

        if ($update) {
            $data = Mentor::where('id', $request->id)->get();
            return $this->responseSuccess('Success update mentor', $data);
        } else {
            return $this->responseError('Failed update mentor', 'Nothing to update');
        }
    }
    /**
     * Delete Mentor
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = Mentor::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}