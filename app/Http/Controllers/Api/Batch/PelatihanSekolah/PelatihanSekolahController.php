<?php

namespace App\Http\Controllers\Api\Batch\PelatihanSekolah;

use App\Http\Controllers\Api\Controller;
use App\Models\BatchPelatihanSekolah;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PelatihanSekolahController extends Controller
{
    /**
     * All Batch Pelatihan Sekolah
     */
    public function batchPelatihanSekolah()
    {
        $data = BatchPelatihanSekolah::get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Batch Pelatihan Sekolah
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'sekolahId' => 'required',
            'batchPelatihanId' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $req = $request->all();
        $batchPelatihanSekolah = new BatchPelatihanSekolah();

        foreach ($req as $key => $values) {
            $batchPelatihanSekolah[$key] = $values;
        }

        if ($batchPelatihanSekolah->save()) {

            $data = BatchPelatihanSekolah::where('id', $batchPelatihanSekolah['id'])->get();
            return $this->responseSuccess('Success create batchPelatihanSekolah', $data);
        } else {
            return $this->responseError('Failed create batchPelatihanSekolah', 'Failed create batchPelatihanSekolah');
        }
    }
    /**
     * Update Batch Pelatihan Sekolah
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'sekolahId' => 'required',
            'batchPelatihanId' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $update = BatchPelatihanSekolah::where('id', $request->id)->update([
            'sekolahId' => $request->sekolahId,
            'batchPelatihanId' => $request->batchPelatihanId,
        ]);
        if ($update) {
            $data = BatchPelatihanSekolah::where('id', $request->id)->get();
            return $this->responseSuccess('Success update batchPelatihanSekolah', $data);
        } else {
            return $this->responseError('Failed update batchPelatihanSekolah', 'Nothing to update');
        }
    }
    /**
     * Delete Batch Pelatihan Sekolah
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = BatchPelatihanSekolah::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}
