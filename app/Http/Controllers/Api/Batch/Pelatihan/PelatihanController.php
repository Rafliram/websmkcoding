<?php

namespace App\Http\Controllers\Api\Batch\Pelatihan;

use App\Http\Controllers\Api\Controller;
use App\Models\BatchPelatihan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PelatihanController extends Controller
{
    /**
     * All Batch Pelatihan
     */
    public function batchPelatihan()
    {
        $data = BatchPelatihan::orderBy('batch')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Create Batch Pelatihan
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'kotaId' => 'required',
            'batch' => 'required',
            'isOffline' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $req = $request->all();
        $batchPelatihan = new BatchPelatihan();

        foreach ($req as $key => $values) {
            $batchPelatihan[$key] = $values;
        }

        if ($batchPelatihan->save()) {

            $data = BatchPelatihan::where('id', $batchPelatihan['id'])->get();
            return $this->responseSuccess('Success create batchPelatihan', $data);
        } else {
            return $this->responseError('Failed create batchPelatihan', 'Failed create batchPelatihan');
        }
    }
    /**
     * Update Batch Pelatihan
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            // 'id' => 'required',
            'kotaId' => 'required',
            'batch' => 'required',
            'isOffline' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $update = BatchPelatihan::where('id', $request->id)->update([
            'kotaId' => $request->kotaId,
            'batch' => $request->batch,
            'isOffline' => $request->isOffline,
        ]);
        if ($update) {
            $data = BatchPelatihan::where('id', $request->id)->get();
            return $this->responseSuccess('Success update batchPelatihan', $data);
        } else {
            return $this->responseError('Failed update batchPelatihan', 'Nothing to update');
        }
    }
    /**
     * Delete Batch Pelatihan
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Prosses Delete
        $delete = BatchPelatihan::where('id', $request->id)->delete();
        if ($delete) {
            return $this->responseSuccess("Successfully deleted", "");
        } else {
            return $this->responseError("Failed deleted", "");
        }
    }
}
