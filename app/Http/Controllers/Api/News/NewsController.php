<?php

namespace App\Http\Controllers\Api\News;

use App\Http\Controllers\Api\Controller;
use App\Models\News;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Date;

class NewsController extends Controller
{
    /**
     * Get all NEWS
     */
    public function news()
    {
        $data = News::orderBy('publishedAt', 'DESC')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Register SMK
     */
    public function create(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'imageUrl' => 'required',
            'content' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }


        // Proses input data
        if ($request->hasFile('imageUrl')) {

            $fileUpload = $request->file('imageUrl');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "NEWS" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/news/', $filename)) {

                $req = $request->all();
                $news = new News();

                foreach ($req as $key => $values) {

                    $news[$key] = $values;
                    if ($key == "imageUrl") {
                        $news[$key] = $this->urlFile($filename, $this->news);
                    }
                }
                // Set DateTime now
                $news['publishedAt'] = $this->dateNow();

                if ($news->save()) {

                    $data = News::where('id', $news['id'])->get();
                    return $this->responseSuccess('Success create news', $data);
                } else {
                    return $this->responseError('Failed create news', 'Failed create news');
                }
            } else {
                return $this->responseError('Failed create news', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed create news", "Request image");
        }
    }
    /**
     * Register SMK
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'imageUrl' => 'required',
            'content' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('imageUrl')) {

            $fileUpload = $request->file('imageUrl');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "NEWS" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('images/news/', $filename)) {

                $delOlderImage = News::where('id', $request->id)->get();
                if (count($delOlderImage) == 1) {

                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR . substr($delOlderImage[0]->imageUrl, -34);
                    $update = News::where('id', $request->id)->update([
                        'title' => $request->title,
                        'description' => $request->description,
                        'imageUrl' => $this->urlFile($filename, $this->news),
                        'content' => $request->content
                    ]);
                    if ($update) {
                        // Remove olderImage
                        unlink($filePath);
                        $data = News::where('id', $request->id)->get();
                        return $this->responseSuccess('Success update news', $data);
                    }
                } else {
                    return $this->responseError('Failed update news', 'Failed update news');
                }
            } else {
                return $this->responseError('Failed update news', "Image does'nt moved ");
            }
        } else {
            return $this->responseError("Failed update news", "Request image");
        }
    }
    /** 
     * Delete news
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        $news = News::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR . substr($news->imageUrl, -34);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = News::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
