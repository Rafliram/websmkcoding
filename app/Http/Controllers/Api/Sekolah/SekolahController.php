<?php

namespace App\Http\Controllers\Api\Sekolah;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Http\Controllers\Api\Controller;
use App\Models\Sekolah;
use Illuminate\Support\Facades\DB;

class SekolahController extends Controller
{
    /**
     * Get all SMK
     */
    public function sekolah()
    {
        $data = Sekolah::orderBy('nama')->get();
        if (!$data) {
            return $this->responseError("Failed get data", "Something whent wrong");
        }
        return $this->responseSuccess("Succesfully get data", $data);
    }
    /**
     * Register SMK
     */
    public function register(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'kotaId' => 'required',
            'nama' => 'required',
            'website' => 'required',
            'jumlahSiswa' => 'required',
            'alamat' => 'required',
            'labKomputer' => 'required',
            'permohonan' => 'required|file',
            'namaPenanggungJawab' => 'required',
            'emailPenanggungJawab' => 'required|email',
            'kontakPenanggungJawab' => 'required',
            'fasilitasLabId' => 'required',
            'softwareDiajarkanId' => 'required',
            'mapelId' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        // Proses input data
        if ($request->hasFile('permohonan')) {
            DB::beginTransaction();
            $fileUpload = $request->file('permohonan');
            date_default_timezone_set("Asia/Jakarta");
            $filename = "PERMOHONAN" . strtoupper(Str::random(12)) . "" . date('Ymdhis') . '.' . $fileUpload->getClientOriginalExtension();

            if ($fileUpload->move('permohonan', $filename)) {
                try {
                    $req = $request->all();

                    foreach ($req as $key => $values) {
                        if ($key == 'fasilitasLabId' || $key == 'softwareDiajarkanId' || $key == 'mapelId') continue;

                        $sekolah[$key] = $values;

                        if ($key == "permohonan") {
                            $sekolah[$key] = $this->urlFile($filename, $this->permohonan);
                        }
                    }

                    $sekolah['createAt'] = $this->dateNow();
                    $sekolah['updateAt'] = $this->dateNow();
                    $saveSekolah = DB::table('sekolah')->insert($sekolah);

                    if ($saveSekolah) {
                        $sekolahId = DB::table('sekolah')->orderBy('id', 'DESC')->first()->id;

                        // Fasilitas Lab
                        $fasilitasArray = json_decode($request->fasilitasLabId);
                        foreach ($fasilitasArray as $value) {
                            $fasilitasLab['fasilitasLabId'] = $value;
                            $fasilitasLab['sekolahId'] = $sekolahId;
                            $savesoFasilitas = DB::table('fasilitas_lab_sekolah')->insert($fasilitasLab);
                            if (!$savesoFasilitas) {
                                DB::rollBack();
                                return $this->responseError('Failed add school', 'Failed add fasilitasId');
                                break;
                            }
                        }

                        $fasilitas = DB::table('fasilitas_lab_sekolah')->where('sekolahId', $sekolahId)->get();
                        for ($j = 0; $j < count($fasilitas); $j++) {
                            $fasilitasId[$j] = $fasilitas[$j]->fasilitasLabId;
                        }


                        // Software Diajarkan
                        $softwareArray = json_decode($request->softwareDiajarkanId);
                        foreach ($softwareArray as $value) {
                            $softwarePembelajaran['softwarePembelajaranId'] = $value;
                            $softwarePembelajaran['sekolahId'] = $sekolahId;
                            $savesoftware = DB::table('software_pembelajaran_sekolah')->insert($softwarePembelajaran);
                            if (!$savesoftware) {
                                DB::rollBack();
                                return $this->responseError('Failed add school', 'Failed add softwareId');
                                break;
                            }
                        }

                        $software = DB::table('software_pembelajaran_sekolah')->where('sekolahId', $sekolahId)->get();
                        for ($i = 0; $i < count($software); $i++) {
                            $softwawareId[$i] = $software[$i]->softwarePembelajaranId;
                        }

                        // Mapel Sekolah
                        $mapelArray = json_decode($request->mapelId);
                        foreach ($mapelArray as $value) {
                            $mapelSekolah['mapelId'] = $value;
                            $mapelSekolah['sekolahId'] = $sekolahId;
                            $saveMapel = DB::table('mapel_sekolah')->insert($mapelSekolah);
                            if (!$saveMapel) {
                                DB::rollBack();
                                return $this->responseError('Failed add school', 'Failed add mapelId');
                                break;
                            }
                        }

                        $mapel = DB::table('mapel_sekolah')->where('sekolahId', $sekolahId)->get();

                        for ($y = 0; $y < count($mapel); $y++) {
                            $mapelId[$y] = $mapel[$y]->mapelId;
                        }

                        $data = Sekolah::where('id', $sekolahId)->get();
                        $data[0]['fasilitasLabId'] = $fasilitasId;
                        $data[0]['softwareDiajarkanId'] = $softwawareId;
                        $data[0]['mapelId'] = $mapelId;

                        DB::commit();
                        return $this->responseSuccess('Success add school', $data);
                    } else {
                        $filePath = public_path() . DIRECTORY_SEPARATOR . 'permohonan' . DIRECTORY_SEPARATOR . $filename;
                        unlink($filePath);
                        return $this->responseError('Failed add school', 'Failed add school');
                    }
                } catch (\Exception $e) {
                    $filePath = public_path() . DIRECTORY_SEPARATOR . 'permohonan' . DIRECTORY_SEPARATOR . $filename;
                    unlink($filePath);
                    DB::rollBack();
                    return $this->responseError('Failed add school', "" . $e);
                }
            } else {
                return $this->responseError('Failed add school', "File does'nt moved ");
            }
        } else {
            return $this->responseError("Failed add school", "Request file permohonan");
        }
    }
    /**
     * Update SMK
     */
    public function update(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'kotaId' => 'required',
            'nama' => 'required',
            'website' => 'required',
            'jumlahSiswa' => 'required',
            'alamat' => 'required',
            'labKomputer' => 'required',
            'namaPenanggungJawab' => 'required',
            'emailPenanggungJawab' => 'required|email',
            'kontakPenanggungJawab' => 'required',
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }

        //Check Sekolah
        if (count(Sekolah::where('id', $request->id)->get()) == 0) {
            return $this->responseError("Failed updated", "Sekolah not registered");
        }

        //Prosses Update
        $update = Sekolah::where('id', $request->id)->update([
            'kotaId' => $request->kotaId,
            'nama' => $request->nama,
            'website' => $request->website,
            'jumlahSiswa' => $request->jumlahSiswa,
            'alamat' => $request->alamat,
            'labKomputer' => $request->labKomputer,
            'namaPenanggungJawab' => $request->namaPenanggungJawab,
            'emailPenanggungJawab' => $request->emailPenanggungJawab,
            'kontakPenanggungJawab' => $request->kontakPenanggungJawab,
            'updateAT' => $this->dateNow()
        ]);

        if ($update) {

            $data = Sekolah::where('id', $request->id)->get()[0];
            return $this->responseSuccess("Successfully updated", $data);
        } else {
            return $this->responseError("Failed updated", "Nothing to update");
        }
    }
    /** 
     * Delete user
     */
    public function delete(Request $request)
    {
        //Validation
        $validate = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->responseError("Data not valid", $validate->errors()->first());
        }


        $sekolah = Sekolah::where('id', $request->id)->get()[0];
        $filePath = public_path() . DIRECTORY_SEPARATOR . 'permohonan' . DIRECTORY_SEPARATOR . substr($sekolah->permohonan, -40);
        if (unlink($filePath)) {
            //Prosses Delete
            $delete = Sekolah::where('id', $request->id)->delete();
            if ($delete) {
                return $this->responseSuccess("Successfully deleted", "");
            } else {
                return $this->responseError("Failed deleted", "");
            }
        } else {
            return $this->responseError("Failed deleted", "Some file not deleted");
        }
    }
}
